package dao;

import dto.TradeDto;
import model.Trade;

import java.util.*;

public class TradeDaoImplStub implements TradeDao {

    private Map<String, Trade> tradesStub;
    private Comparator<String> tradeIdComparator;

    public TradeDaoImplStub() {
    	/* 
    	 * Trade IDs are numbers saved as Strings ("1", "2", "3"...), 
    	 * so we order IDs first by length, then by natural order of Strings,
    	 * to avoid the default ordering "1", "10", "2", "3"... (only by natural order of Strings) 
    	 */
    	tradeIdComparator = Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder());
    	tradesStub = new TreeMap<>(tradeIdComparator);
    }
    
    @Override
    public String addTrade(TradeDto tradeDto) {
        if (tradeDto == null) return null;
        String freeTradeId = getFreeTradeId();

        Trade trade = new Trade(freeTradeId, tradeDto.getExecutionTime(),
                tradeDto.getQuantityFilled(), tradeDto.getExecutedPrice()
                , tradeDto.getSellOrderID(), tradeDto.getBuyOrderID());

        Optional<Trade> put =Optional.ofNullable(tradesStub.put(freeTradeId, trade));
        if (!put.isPresent()) {
            return freeTradeId;
        }
        return null;
    }


    @Override
    public Trade removeTrade(String tradeRemoveId) {
        return tradesStub.remove(tradeRemoveId);
    }

    @Override
    public Trade editTrade(String tradeEditedId, Trade tradeEdited) {

        return tradesStub.replace(tradeEditedId,tradeEdited);
    }

    @Override
    public List<Trade> getAllTrades() {
        return new ArrayList<>(tradesStub.values());
    }

    @Override
    public Trade getTradeById(String tradeId) {
        return tradesStub.get(tradeId);
    }

    private String getFreeTradeId() {
        if (tradesStub.isEmpty()) {
            return "1";
        }
        
        // Initialize TreeSet with comparator, to get the correct last ID
        TreeSet<String> takenIds = new TreeSet<>(tradeIdComparator);
        takenIds.addAll(tradesStub.keySet());
        
        String lastId = takenIds.last();
        int digit = Integer.parseInt(lastId);
        digit++;
        return String.valueOf(digit);
    }

}
