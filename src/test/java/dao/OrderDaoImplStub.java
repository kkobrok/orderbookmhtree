package dao;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import dto.OrderDto;
import model.Order;
import model.OrderType;

public class OrderDaoImplStub implements OrderDao {
    
    private Map<String, Order> buyOrders;
    private Map<String, Order> sellOrders;
    private Comparator<String> orderIdComparator;
    
    public OrderDaoImplStub() {
    	/* 
    	 * Orders IDs are numbers with a prefix ("BORD1", "BORD2", "BORD3"...), 
    	 * so we order IDs first by length (ascending), 
    	 * then by alphabetical order (ascending),
    	 * to avoid the default ordering "BORD1", "BORD10", "BORD2", "BORD3"... 
    	 * (only by alphabetical order) 
    	 */
    	orderIdComparator = Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder());
        buyOrders = new TreeMap<>(orderIdComparator);
        sellOrders = new TreeMap<>(orderIdComparator);
    }
    
    @Override
    public String addBuyOrder(OrderDto buyOrderDto) {
        if (buyOrderDto == null) return null;
        
        // Create order with new ID
        Order buyOrder = new Order(getFreeBuyOrderId());
        
        // Copy DTO fields to new order
        buyOrder.setOrderType(buyOrderDto.getOrderType());
        buyOrder.setPrice(buyOrderDto.getPrice());
        buyOrder.setQuantity(buyOrderDto.getQuantity());
        
        // Add new order to collection
        buyOrders.put(buyOrder.getOrderID(), buyOrder);
        
        // Return assigned ID
        return buyOrder.getOrderID();
    }

    @Override
    public String addSellOrder(OrderDto sellOrderDto) {
        if (sellOrderDto == null) return null;
        
        // Create order with new ID
        Order sellOrder = new Order(getFreeSellOrderId());
        
        // Copy DTO fields to new order
        sellOrder.setOrderType(sellOrderDto.getOrderType());
        sellOrder.setPrice(sellOrderDto.getPrice());
        sellOrder.setQuantity(sellOrderDto.getQuantity());
        
        // Add new order to collection
        sellOrders.put(sellOrder.getOrderID(), sellOrder);
        
        // Return assigned ID
        return sellOrder.getOrderID();
    }

    @Override
    public Order removeBuyOrder(String buyOrderId) {
        if (buyOrderId == null) return null;
        return buyOrders.remove(buyOrderId);
    }

    @Override
    public Order removeSellOrder(String sellOrderId) {
        if (sellOrderId == null) return null;
        return sellOrders.remove(sellOrderId);
    }

    @Override
    public Order editBuyOrder(String buyOrderId, Order buyOrder) {
        if (buyOrderId == null || buyOrder == null) return null;
        return buyOrders.replace(buyOrderId, buyOrder);
    }

    @Override
    public Order editSellOrder(String sellOrderId, Order sellOrder) {
        if (sellOrderId == null || sellOrder == null) return null;
        return sellOrders.replace(sellOrderId, sellOrder);
    }

    @Override
    public List<Order> getAllBuyOrders() {
        return new ArrayList<>(buyOrders.values());
    }

    @Override
    public List<Order> getAllSellOrders() {
        return new ArrayList<>(sellOrders.values());
    }

    private String getFreeBuyOrderId() {
        if (buyOrders.isEmpty()) {
            return OrderType.BUY.getPrefix() + 1;
        }
        
        // Initialize TreeSet with comparator, to get the correct last ID
        TreeSet<String> takenIds = new TreeSet<>(orderIdComparator);
        takenIds.addAll(buyOrders.keySet());

        String lastId = takenIds.last();
        String digitPart = lastId.substring(OrderType.BUY.getPrefix().length()); // Remove alphabetic prefix
        int digit = Integer.parseInt(digitPart);
        return OrderType.BUY.getPrefix() + (++digit);
    }
    
    private String getFreeSellOrderId() {
        if (sellOrders.isEmpty()) {
            return OrderType.SELL.getPrefix() + 1;
        }
        
        // Initialize TreeSet with comparator, to get the correct last ID
        TreeSet<String> takenIds = new TreeSet<>(orderIdComparator);
        takenIds.addAll(sellOrders.keySet());
        
        String lastId = takenIds.last();
        String digitPart = lastId.substring(OrderType.SELL.getPrefix().length()); // Remove alphabetic prefix
        int digit = Integer.parseInt(digitPart);
        return OrderType.SELL.getPrefix() + (++digit);
    }

    @Override
    public Order getBuyOrder(String buyOrderId) {
        return buyOrders.get(buyOrderId);
    }

    @Override
    public Order getSellOrder(String sellOrderId) {
        return sellOrders.get(sellOrderId);
    }
}

