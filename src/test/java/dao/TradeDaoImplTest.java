package dao;

import dto.TradeDto;
import exceptions.OrderbookPersistenceException;
import model.Trade;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

class TradeDaoImplTest {

    private TradeDao tradeDaoTest;
    private Trade testTradeOne;
    private Trade testTradeTwo;
    private Trade testTradeTwoEdited;
    private TradeDto testTradeDtoOne;
    private TradeDto testTradeDtoTwo;


    @org.junit.jupiter.api.BeforeEach
    void setUp() throws IOException {
        FileWriter fileWriter = new FileWriter("src/test/resources/TradeTest.txt");
        fileWriter.close(); // Overwrite file so for every test file are empty
        tradeDaoTest = new TradeDaoImpl("src/test/resources/TradeTest.txt");

        testTradeDtoOne = new TradeDto();
        testTradeDtoOne.setExecutedPrice(new BigDecimal(10));
        testTradeDtoOne.setQuantityFilled(30);
        testTradeDtoOne.setExecutionTime(LocalDateTime.of(2020, 12, 1, 0, 0, 0));
        testTradeDtoOne.setBuyOrderID("BORD1");
        testTradeDtoOne.setSellOrderID("SORD1");

        testTradeDtoTwo = new TradeDto();
        testTradeDtoTwo.setExecutedPrice(new BigDecimal(25));
        testTradeDtoTwo.setQuantityFilled(90);
        testTradeDtoTwo.setExecutionTime(LocalDateTime.of(2021, 1, 1, 0, 0, 0));
        testTradeDtoTwo.setBuyOrderID("BORD2");
        testTradeDtoTwo.setSellOrderID("SORD2");

        testTradeOne = new Trade("1");
        testTradeOne.setExecutedPrice(new BigDecimal(10));
        testTradeOne.setQuantityFilled(30);
        testTradeOne.setExecutionTime(LocalDateTime.of(2020, 12, 1, 0, 0, 0));
        testTradeOne.setBuyOrderID("BORD1");
        testTradeOne.setSellOrderID("SORD1");

        testTradeTwo = new Trade("2");
        testTradeTwo.setExecutedPrice(new BigDecimal(25));
        testTradeTwo.setQuantityFilled(90);
        testTradeTwo.setExecutionTime(LocalDateTime.of(2021, 1, 1, 0, 0, 0));
        testTradeTwo.setBuyOrderID("BORD2");
        testTradeTwo.setSellOrderID("SORD2");

        testTradeTwoEdited = new Trade("2");
        testTradeTwoEdited.setExecutedPrice(new BigDecimal(50));
        testTradeTwoEdited.setQuantityFilled(200);
        testTradeTwoEdited.setExecutionTime(LocalDateTime.of(2021, 1, 1, 0, 0, 0));
        testTradeTwoEdited.setBuyOrderID("BORD2");
        testTradeTwoEdited.setSellOrderID("SORD2");

    }


    @org.junit.jupiter.api.Test
    void addTrade() throws OrderbookPersistenceException {
        String newTradeID = tradeDaoTest.addTrade(testTradeDtoOne);
        Trade tradeById = tradeDaoTest.getTradeById(testTradeOne.getTradeID());
        Assertions.assertNotNull(newTradeID);
        Assertions.assertNotNull(newTradeID,"1");
        Assertions.assertEquals(tradeById.hashCode(), testTradeOne.hashCode());
        Assertions.assertEquals(tradeById.getTradeID(), "1");
        Assertions.assertEquals(tradeById.getExecutedPrice(), new BigDecimal(10));
        Assertions.assertEquals(tradeById.getExecutionTime(), LocalDateTime.of(2020, 12, 1, 0, 0, 0));
        Assertions.assertEquals(tradeById.getQuantityFilled(), 30);
    }

    @org.junit.jupiter.api.Test
    void removeTrade() throws OrderbookPersistenceException {
        tradeDaoTest.addTrade(testTradeDtoOne);
        Trade tradeRemoved = tradeDaoTest.removeTrade(testTradeOne.getTradeID());
        Assertions.assertEquals(tradeRemoved.hashCode(), testTradeOne.hashCode());
        Assertions.assertNull(tradeDaoTest.getTradeById(testTradeOne.getTradeID()));
    }

    @org.junit.jupiter.api.Test
    void editTrade() throws OrderbookPersistenceException {
        tradeDaoTest.addTrade(testTradeDtoOne);
        tradeDaoTest.addTrade(testTradeDtoTwo);
        Trade tradeEdited = tradeDaoTest.editTrade(testTradeTwo.getTradeID(), testTradeTwoEdited);
        Trade tradeEditedNull = tradeDaoTest.editTrade("99", testTradeOne);
        Trade tradeById = tradeDaoTest.getTradeById(testTradeTwoEdited.getTradeID());


        Assertions.assertEquals(testTradeTwo.hashCode(), tradeEdited.hashCode());
        Assertions.assertNull(tradeEditedNull);
        Assertions.assertEquals(tradeById.hashCode(), testTradeTwoEdited.hashCode());
        Assertions.assertEquals(tradeById.getTradeID(), "2");
        Assertions.assertEquals(tradeById.getExecutedPrice(), new BigDecimal(50));
        Assertions.assertEquals(tradeById.getExecutionTime(), LocalDateTime.of(2021, 1, 1, 0, 0, 0));
        Assertions.assertEquals(tradeById.getQuantityFilled(), 200);

    }

    @org.junit.jupiter.api.Test
    void getAllTrades() throws OrderbookPersistenceException {
        tradeDaoTest.addTrade(testTradeDtoOne);
        tradeDaoTest.addTrade(testTradeDtoTwo);
        List<Trade> allTrades = tradeDaoTest.getAllTrades();
        Assertions.assertNotNull(allTrades);
        Assertions.assertEquals(allTrades.size(), 2);

    }

    @Test
    void getTradeById() throws OrderbookPersistenceException {
        //For tests coverage
        //Same as in addTrade
        tradeDaoTest.addTrade(testTradeDtoOne);
        Trade tradeById = tradeDaoTest.getTradeById(testTradeOne.getTradeID());
        Assertions.assertEquals(tradeById.hashCode(), testTradeOne.hashCode());
        Assertions.assertEquals(tradeById.getTradeID(), "1");
        Assertions.assertEquals(tradeById.getExecutedPrice(), new BigDecimal(10));
        Assertions.assertEquals(tradeById.getExecutionTime(), LocalDateTime.of(2020, 12, 1, 0, 0, 0));
        Assertions.assertEquals(tradeById.getQuantityFilled(), 30);


    }
}