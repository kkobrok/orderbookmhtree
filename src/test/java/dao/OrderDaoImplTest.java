/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;


import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import model.OrderType;
import model.Order;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import dto.OrderDto;
import exceptions.OrderbookPersistenceException;

import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author noelia, ishar (pair programming)
 */
public class OrderDaoImplTest {
    
    OrderDao orderDao;
    OrderDto buyOrderDto1, buyOrderDto2, sellOrderDto1, sellOrderDto2;
    Order originalBuyOrder1, editedBuyOrder1, originalSellOrder1, editedSellOrder1;
    
    public OrderDaoImplTest() {
    	
    	// Test input 1 - Buy order 1
    	buyOrderDto1 = new OrderDto(new BigDecimal("190.00"), 20, OrderType.BUY);
    	originalBuyOrder1 = new Order("BORD1", new BigDecimal("190.00"), 20, OrderType.BUY);
    	editedBuyOrder1 = new Order("BORD1", new BigDecimal("190.01"), 21, OrderType.BUY);
    	
    	// Test input 2 - Sell order 1
    	sellOrderDto1 = new OrderDto(new BigDecimal("190.00"), 20, OrderType.SELL);
    	originalSellOrder1 = new Order("SORD1", new BigDecimal("190.00"), 20, OrderType.SELL);
    	editedSellOrder1 = new Order("SORD1", new BigDecimal("190.01"), 21, OrderType.SELL);
    	
    	// Test input 3 - Buy order 2
    	buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
    	
    	// Test input 4 - Sell order 2
    	sellOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.SELL);
    }
    
    @BeforeEach
    public void setUp() throws IOException {
        FileWriter fileWriter = new FileWriter("src/test/resources/OrderTest.txt");
        fileWriter.close(); // Overwrite file so for every test file are empty
        orderDao = new OrderDaoImpl("src/test/resources/OrderTest.txt");
    }

    @Test
    public void testGetAllEmptyCollection() throws OrderbookPersistenceException {
    	// ACT & ASSERT
        assertTrue(orderDao.getAllBuyOrders().isEmpty());
        assertTrue(orderDao.getAllSellOrders().isEmpty());
    }
    
    @Test
    public void testGetAllNonEmptyCollection() throws OrderbookPersistenceException {
        // ARRANGE
    	// Previously add 2 orders to each collection
        orderDao.addBuyOrder(buyOrderDto1);
        orderDao.addBuyOrder(buyOrderDto2);

        orderDao.addSellOrder(sellOrderDto1);
        orderDao.addSellOrder(sellOrderDto2);
        
        // ACT & ASSERT
        assertEquals(2, orderDao.getAllBuyOrders().size());
        assertEquals(2, orderDao.getAllSellOrders().size());
    }
    
    @Test
    public void testAddNullOrder() throws OrderbookPersistenceException {
    	// ACT & ASSERT
        assertNull(orderDao.addBuyOrder(null));
        assertTrue(orderDao.getAllBuyOrders().isEmpty());
        
        assertNull(orderDao.addSellOrder(null));
        assertTrue(orderDao.getAllSellOrders().isEmpty());
    }
    
    @Test
    public void testAddNonNullOrder() throws OrderbookPersistenceException {
    	// ACT
        String idBuyOrder1 = orderDao.addBuyOrder(buyOrderDto1);
        String idSellOrder1 = orderDao.addSellOrder(sellOrderDto1);
        
    	// ASSERT
        // Check it returns generated ID
        assertNotNull(idBuyOrder1);
        assertNotNull(idSellOrder1);
        
        // Check order is saved correctly
        assertEquals(originalBuyOrder1, orderDao.getBuyOrder(idBuyOrder1));
        assertEquals(originalSellOrder1, orderDao.getSellOrder(idSellOrder1));
    }
    
    @Test
    public void testRemoveOrderNotFound() throws OrderbookPersistenceException {
    	// ACT & ASSERT
        assertNull(orderDao.removeBuyOrder("BORD1"));
        assertNull(orderDao.removeSellOrder("SORD1"));
    }
    
    @Test
    public void testRemoveOrderFound() throws OrderbookPersistenceException {
    	// ARRANGE
    	// Previously add 1 order to each collection
        orderDao.addBuyOrder(buyOrderDto1);
        orderDao.addSellOrder(sellOrderDto1);
        
        // ACT & ASSERT
        assertEquals(originalBuyOrder1, orderDao.removeBuyOrder("BORD1"));
        assertTrue(orderDao.getAllBuyOrders().isEmpty());
        
        assertEquals(originalSellOrder1, orderDao.removeSellOrder("SORD1"));
        assertTrue(orderDao.getAllSellOrders().isEmpty());
    }
    
    @Test
    public void testEditOrderNotFound() throws OrderbookPersistenceException {
    	// ACT & ASSERT
        assertNull(orderDao.editBuyOrder("BORD1", editedBuyOrder1));
        assertNull(orderDao.editSellOrder("SORD1", editedSellOrder1));
    }
    
    @Test
    public void testEditOrderFoundWithNull() throws OrderbookPersistenceException {
    	// ARRANGE
    	// Previously add 1 order to each collection
        orderDao.addBuyOrder(buyOrderDto1);
        orderDao.addSellOrder(sellOrderDto1);
        
        // ACT & ASSERT
        assertNull(orderDao.editBuyOrder("BORD1", null));
        assertNull(orderDao.editSellOrder("SORD1", null));
    }
    
    @Test
    public void testEditOrderFoundWithOrder() throws OrderbookPersistenceException {
    	// ARRANGE
    	// Previously add 1 order to each collection
        orderDao.addBuyOrder(buyOrderDto1);
        orderDao.addSellOrder(sellOrderDto1);
        
        // ACT & ASSERT
        // Check it returns the replaced order
        assertEquals(originalBuyOrder1, orderDao.editBuyOrder("BORD1", editedBuyOrder1));
        assertEquals(originalSellOrder1, orderDao.editSellOrder("SORD1", editedSellOrder1));
        
        // Check it updates it on the collection 
        assertEquals(editedBuyOrder1, orderDao.getBuyOrder("BORD1"));
        assertEquals(editedSellOrder1, orderDao.getSellOrder("SORD1"));
    }
}
