package service;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import exceptions.OrderbookPersistenceException;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.OrderDao;
import dao.OrderDaoImplStub;
import dao.TradeDao;
import dao.TradeDaoImplStub;
import dto.OrderDto;
import dto.OrderbookStats;
import dto.TradeDto;
import exceptions.InvalidOrderException;
import exceptions.OrderNotFoundException;
import model.Order;
import model.OrderType;
import model.Trade;

class OrderbookServiceTest {
	
	OrderbookService service;
	OrderDao orderDaoStub;
	TradeDao tradeDaoStub;

	public OrderbookServiceTest() {
		// Service dependency injection with Spring 
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationConfiguration.xml");
        service = ctx.getBean("service", OrderbookServiceImpl.class);
        orderDaoStub = ctx.getBean("orderDao", OrderDaoImplStub.class);
        tradeDaoStub = ctx.getBean("tradeDao", TradeDaoImplStub.class);
	}
	
	@Test
	void testMatchOneFullyFilled() throws OrderbookPersistenceException {
		// ARRANGE
		/*         
			BID SIDE            	ASK SIDE     
			QUANTITY	PRICE		PRICE	QUANTITY 
			30			190.26		190.25	30
			30			190.25		190.26	30
		*/
		
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);
        
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);
		
		// ACT & ASSERT
		// Check a trade is returned
		assertNotNull(service.match());
		
		// Check top sell and buy orders were fully filled (removed)
		assertNull(service.getBuyOrderById("BORD1"));
		assertNull(service.getSellOrderById("SORD1"));
	}
	
	@Test
	void testMatchOnePartiallyFilledBuySide() throws OrderbookPersistenceException {
		// ARRANGE
		/*         
			BID SIDE            	ASK SIDE     
			QUANTITY	PRICE		PRICE	QUANTITY 
			40			190.26		190.25	30
			30			190.25		190.26	30
		*/
		
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 40, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);
        
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);
		
		// ACT & ASSERT
		// Check a trade is returned
		assertNotNull(service.match());
		
		// Check top buy order was partially filled with the correct amount
		assertEquals(10, service.getBuyOrderById("BORD1").getQuantity());
		
		// Check top sell order was fully filled
		assertNull(service.getSellOrderById("SORD1"));
	}
	
	@Test
	void testMatchOnePartiallyFilledSellSide() throws OrderbookPersistenceException {
		// ARRANGE
		/*         
			BID SIDE            	ASK SIDE     
			QUANTITY	PRICE		PRICE	QUANTITY 
			30			190.26		190.25	40
			30			190.25		190.26	30
		*/
		
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);
        
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.25"), 40, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);
		
		// ACT & ASSERT
		// Check a trade is returned
		assertNotNull(service.match());
		
		// Check top buy order was fully filled
		assertNull(service.getBuyOrderById("BORD1"));
		
		// Check top sell order was partially filled with the correct amount
		assertEquals(10, service.getSellOrderById("SORD1").getQuantity());
	}
	
	@Test
	void testMatchOneBelowMinimumSpread() throws OrderbookPersistenceException {
		// ARRANGE
		/*         
			BID SIDE            	ASK SIDE     
			QUANTITY	PRICE		PRICE	QUANTITY 
			30			190.00		190.11	30			
		*/
		
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.00"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.11"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);
		
		// ACT & ASSERT
		// Check a trade is not returned
		assertNull(service.match());
		
		// Check top sell and buy orders were not filled (updated or removed)
		assertNotNull(service.getBuyOrderById("BORD1"));
		assertNotNull(service.getSellOrderById("SORD1"));
		
		assertEquals(service.getBuyOrderById("BORD1").getQuantity(), buyOrderDto1.getQuantity());
		assertEquals(service.getSellOrderById("SORD1").getQuantity(), sellOrderDto1.getQuantity());
	}
	
	@Test
	void testMatchOneNoOrders()throws OrderbookPersistenceException {
		// ACT & ASSERT
		// Check a trade is not returned
		assertNull(service.match());
	}
	
	@Test
	void testMatchAllNoOrders() throws OrderbookPersistenceException{
		// ACT & ASSERT
		// Check an empty list of trades is returned
		assertTrue(service.matchAll().isEmpty());
	}
	
	@Test
	void testMatchOneNoBuyOrders()throws OrderbookPersistenceException {
		// ARRANGE
		// Add buy order
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		// ACT & ASSERT
		// Check a trade is not returned
		assertNull(service.match());
	}
	
	@Test
	void testMatchAllNoBuyOrders() throws OrderbookPersistenceException {
		// ARRANGE
		// Add buy order
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		// ACT & ASSERT
		// Check an empty list of trades is returned
		assertTrue(service.matchAll().isEmpty());
	}
	
	@Test
	void testMatchOneNoSellOrders() throws OrderbookPersistenceException {
		// ARRANGE
		// Add sell order
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("191"), 30, OrderType.SELL);
        orderDaoStub.addSellOrder(sellOrderDto1);
        
		// ACT & ASSERT
		// Check a trade is not returned
		assertNull(service.match());
	}
	
	@Test
	void testMatchAllNoSellOrders() throws OrderbookPersistenceException {
		// ARRANGE
		// Add sell order
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("191"), 30, OrderType.SELL);
        orderDaoStub.addSellOrder(sellOrderDto1);
        
		// ACT & ASSERT
		// Check an empty list of trades is returned
		assertTrue(service.matchAll().isEmpty());
	}
	
	@Test
	void testMatchAllWhenAllAboveMinSpread() throws OrderbookPersistenceException {

		// ARRANGE
		/*         
			BID SIDE            	ASK SIDE     
			QUANTITY	PRICE		PRICE	QUANTITY 
			30			190.26		190.25	30
			30			190.25		190.26	30
		*/
		
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);
        
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.26"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);
		
		// ACT & ASSERT
		// Check a list of 2 trades is returned
		assertEquals(2, service.matchAll().size());
	}
	
	
	@Test
	void testMatchAllWhenSomeBelowMinSpread() throws OrderbookPersistenceException {

		// ARRANGE
		/*         
			BID SIDE            	ASK SIDE     
			QUANTITY	PRICE		PRICE	QUANTITY 
			30			190.00		190.10	30			<- Filled
			30			190.00		190.11	30			<- Unfilled
		*/
		
        OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.00"), 30, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.00"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);
        
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.10"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.11"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);
		
		// ACT & ASSERT
		// Check a list of 1 trade is returned
		assertEquals(1, service.matchAll().size());
	}
	
	@Test
	void testGetTradeByIdFound() throws OrderbookPersistenceException {
		// ARRANGE
		// Add trade to DAO
		TradeDto tradeDto = new TradeDto(LocalDateTime.now(), 20, new BigDecimal("190"), "SORD1", "BORD2");
		tradeDaoStub.addTrade(tradeDto);
		
		// ACT
		Trade returnedTrade = service.getTradeById("1");
		
		// ASSERT
		assertNotNull(returnedTrade);
		assertEquals(returnedTrade.getExecutionTime(), tradeDto.getExecutionTime());
		assertEquals(returnedTrade.getQuantityFilled(), tradeDto.getQuantityFilled());
		assertEquals(returnedTrade.getSellOrderID(), tradeDto.getSellOrderID());
		assertEquals(returnedTrade.getBuyOrderID(), tradeDto.getBuyOrderID());
	}
	
	@Test
	void testGetTradeByIdNotFound() throws OrderbookPersistenceException {
		// ACT & ASSERT
		assertNull(service.getTradeById("1"));
	}
	
	@Test
	void testGetAllTradesSortedByExecutionTimeNoTrades() throws OrderbookPersistenceException {
		// ACT & ASSERT
		assertTrue(service.getAllTradesSortedByExecutionTime().isEmpty());
	}
	
	@Test
	void testGetAllTradesSortedByExecutionTimeSeveralTrades() throws OrderbookPersistenceException {
		// ARRANGE
		// Add 2 trades to DAO
		TradeDto tradeDto1 = new TradeDto(LocalDateTime.now().minusMinutes(1), 20, new BigDecimal("190"), "SORD1", "BORD2");
		TradeDto tradeDto2 = new TradeDto(LocalDateTime.now(), 20, new BigDecimal("190"), "SORD2", "BORD3");
		
		tradeDaoStub.addTrade(tradeDto1);
		tradeDaoStub.addTrade(tradeDto2);
		
		// Create equivalent Trades to compare
		Trade trade1 = new Trade("1", tradeDto1.getExecutionTime(), 20, new BigDecimal("190"), "SORD1", "BORD2");
		Trade trade2 = new Trade("2", tradeDto2.getExecutionTime(), 20, new BigDecimal("190"), "SORD2", "BORD3");
		
		// ACT
		List<Trade> trades = service.getAllTradesSortedByExecutionTime();
		
		// ASSERT
		assertFalse(trades.isEmpty());
		assertTrue(trades.get(0).equals(trade2), "trade2 should appear before trade1 (most recent execution time)");
		assertTrue(trades.get(1).equals(trade1), "trade1 should appear after trade2 (least recent execution time)");
	}
	
	@Test
	void testGetAllBuyOrdersNoOrders() throws OrderbookPersistenceException {
		// ACT & ASSERT
		assertTrue(service.getAllBuyOrders().isEmpty());
	}
	
	@Test
	void testGetAllBuyOrdersSeveralOrders() throws OrderbookPersistenceException {
		// ARRANGE
		// Add 2 buy orders to DAO
		OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 40, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);
        
		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);
		
		// Create equivalent Orders to compare
		Order buyOrder1 = new Order("BORD1", new BigDecimal("190.26"), 40, OrderType.BUY);
		Order buyOrder2 = new Order("BORD2", new BigDecimal("190.25"), 30, OrderType.BUY);

		// ACT
		List<Order> buyOrders = service.getAllBuyOrders();
		
		// ASSERT
		assertFalse(buyOrders.isEmpty());
		assertEquals(buyOrders.get(0), buyOrder1);
		assertEquals(buyOrders.get(1), buyOrder2);
	}
	
	@Test
	void testGetAllSellOrdersNoOrders() throws OrderbookPersistenceException {
		// ACT & ASSERT
		assertTrue(service.getAllSellOrders().isEmpty());
	}
	
	@Test
	void testGetAllSellOrdersSeveralOrders() throws OrderbookPersistenceException {
		// ARRANGE
		// Add 2 sell orders to DAO
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.10"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.11"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);
		
		// Create equivalent Orders to compare
		Order sellOrder1 = new Order("SORD1", new BigDecimal("190.10"), 30, OrderType.SELL);
		Order sellOrder2 = new Order("SORD2", new BigDecimal("190.11"), 30, OrderType.SELL);

		// ACT
		List<Order> sellOrders = service.getAllSellOrders();
		
		// ASSERT
		assertFalse(sellOrders.isEmpty());
		assertEquals(sellOrders.get(0), sellOrder1);
		assertEquals(sellOrders.get(1), sellOrder2);
	}

	@Test
	void testGetOrderbookStatsNoOrders() throws OrderbookPersistenceException {
		// ACT
		OrderbookStats stats = service.getOrderbookStats();

		// ASSERT
		assertEquals(stats.getNumberOfSellOrders(), 0);
		assertEquals(stats.getNumberOfBuyOrders(), 0);
		assertEquals(stats.getOverallSellQuantity(), 0);
		assertEquals(stats.getOverallBuyQuantity(), 0);
		assertEquals(stats.getAverageSellPrice(), BigDecimal.ZERO);
		assertEquals(stats.getAverageBuyPrice(), BigDecimal.ZERO);
	}

	@Test
	void testGetOrderbookStatsOneOrderOfEachType() throws OrderbookPersistenceException {
		// ARRANGE

		// Add 1 buy order to DAO
		OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 40, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);

		// Add 1 sell order to DAO
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.10"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		// ACT
		OrderbookStats stats = service.getOrderbookStats();

		// ASSERT
		assertEquals(stats.getNumberOfSellOrders(), 1);
		assertEquals(stats.getNumberOfBuyOrders(), 1);
		assertEquals(stats.getOverallSellQuantity(), sellOrderDto1.getQuantity());
		assertEquals(stats.getOverallBuyQuantity(), buyOrderDto1.getQuantity());
		assertEquals(stats.getAverageSellPrice(), sellOrderDto1.getPrice());
		assertEquals(stats.getAverageBuyPrice(), buyOrderDto1.getPrice());
	}

	@Test
	void testGetOrderbookStatsSeveralOrdersOfEachType() throws OrderbookPersistenceException {
		// ARRANGE

		// Add 2 buy orders to DAO
		OrderDto buyOrderDto1 = new OrderDto(new BigDecimal("190.26"), 40, OrderType.BUY);
        orderDaoStub.addBuyOrder(buyOrderDto1);

		OrderDto buyOrderDto2 = new OrderDto(new BigDecimal("190.25"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto2);

		// Add 2 sell orders to DAO
        OrderDto sellOrderDto1 = new OrderDto(new BigDecimal("190.10"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto1);

		OrderDto sellOrderDto2 = new OrderDto(new BigDecimal("190.11"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(sellOrderDto2);

		// Calculate expected average prices
		BigDecimal averageBuyPrice =
				( new BigDecimal("190.26").add(new BigDecimal("190.25")) )
				.divide(new BigDecimal("2"), RoundingMode.HALF_EVEN);

		BigDecimal averageSellPrice =
				( new BigDecimal("190.10").add(new BigDecimal("190.11")) )
				.divide(new BigDecimal("2"), RoundingMode.HALF_EVEN);

		// ACT
		OrderbookStats stats = service.getOrderbookStats();

		// ASSERT
		assertEquals(stats.getNumberOfSellOrders(), 2);
		assertEquals(stats.getNumberOfBuyOrders(), 2);
		assertEquals(stats.getOverallSellQuantity(), sellOrderDto1.getQuantity() + sellOrderDto2.getQuantity());
		assertEquals(stats.getOverallBuyQuantity(), buyOrderDto1.getQuantity() + buyOrderDto2.getQuantity());
		assertEquals(stats.getAverageSellPrice(), averageSellPrice);
		assertEquals(stats.getAverageBuyPrice(), averageBuyPrice);
	}

	@Test
	void testAddBuyOrderNull() throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			assertNull(service.addBuyOrder(null));
		} catch (InvalidOrderException e) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testAddSellOrderNull() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			assertNull(service.addSellOrder(null));
		} catch (InvalidOrderException e) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testAddBuyOrderPriceWellUnder190()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("1"), 40, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderPriceWellUnder190() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("1"), 40, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddBuyOrderPriceRightUnder190() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("189.99999999"), 40, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testSellBuyOrderPriceRightUnder190() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("189.99999999"), 40, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderPriceRightOver191() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("191.0000001"), 40, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddBuyOrderPriceRightOver191() throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("191.0000001"), 40, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderPriceWellOver191() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("5845437"), 40, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

///
	@Test
	void testAddBuyOrderQuantityWellUnder20() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("190"), 0, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderQuantityWellUnder20() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("190"), 0, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddBuyOrderQuantityRightUnder20()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("190"), 19, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderQuantityRightUnder20()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("190"), 19, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddBuyOrderQuantityRightOver50() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("190"), 51, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderQuantityRightOver50() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("190"), 51, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddBuyOrderQuantityWellOver50() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("190"), 342433, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderQuantityWellOver50() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("190"), 342433, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}


	@Test
	void testAddBuyOrderPriceExactly190QuantityExactly20() throws OrderbookPersistenceException{
		// ARRANGE
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 20, OrderType.BUY);
		// ACT & ASSERT
		try {
			Order addedOrder = service.addBuyOrder(orderDto);
			assertNotNull(addedOrder);
			assertTrue(addedOrder.getOrderID().equals("BORD1"));
			assertEquals(addedOrder.getPrice(), orderDto.getPrice());
			assertEquals(addedOrder.getQuantity(), orderDto.getQuantity());
			assertEquals(addedOrder.getOrderType(), orderDto.getOrderType());
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testAddSellOrderPriceExactly190QuantityExactly20() throws OrderbookPersistenceException{
		// ARRANGE
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 20, OrderType.SELL);
		// ACT & ASSERT
		try {
			Order addedOrder = service.addSellOrder(orderDto);
			assertNotNull(addedOrder);
			assertTrue(addedOrder.getOrderID().equals("SORD1"));
			assertEquals(addedOrder.getPrice(), orderDto.getPrice());
			assertEquals(addedOrder.getQuantity(), orderDto.getQuantity());
			assertEquals(addedOrder.getOrderType(), orderDto.getOrderType());
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testAddBuyOrderPriceExactly191QuantityExactly50()throws OrderbookPersistenceException {
		// ARRANGE
		OrderDto orderDto = new OrderDto(new BigDecimal("191"), 50, OrderType.SELL);
		// ACT & ASSERT
		try {
			Order addedOrder = service.addSellOrder(orderDto);
			assertNotNull(addedOrder);
			assertTrue(addedOrder.getOrderID().equals("SORD1"));
			assertEquals(addedOrder.getPrice(), orderDto.getPrice());
			assertEquals(addedOrder.getQuantity(), orderDto.getQuantity());
			assertEquals(addedOrder.getOrderType(), orderDto.getOrderType());
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testAddSellOrderPriceExactly191QuantityExactly50() throws OrderbookPersistenceException{
		// ARRANGE
		OrderDto orderDto = new OrderDto(new BigDecimal("191"), 50, OrderType.SELL);
		// ACT & ASSERT
		try {
			Order addedOrder = service.addSellOrder(orderDto);
			assertNotNull(addedOrder);
			assertTrue(addedOrder.getOrderID().equals("SORD1"));
			assertEquals(addedOrder.getPrice(), orderDto.getPrice());
			assertEquals(addedOrder.getQuantity(), orderDto.getQuantity());
			assertEquals(addedOrder.getOrderType(), orderDto.getOrderType());
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testAddBuyOrderSellType() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addBuyOrder(new OrderDto(new BigDecimal("190"), 50, OrderType.SELL));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testAddSellOrderBuyType() throws OrderbookPersistenceException{
		// ACT & ASSERT
		try {
			service.addSellOrder(new OrderDto(new BigDecimal("190"), 50, OrderType.BUY));
			fail("Should have thrown InvalidOrderException");
		} catch(InvalidOrderException ex) {
		}
	}

	// EDIT ORDER TESTS

	@Test
	void testEditBuyOrderNullId() throws OrderbookPersistenceException{
		// ARRANGE
		// Create edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("191"), 40, OrderType.BUY);

		// ACT & ASSERT
		try {
			assertNull(service.editBuyOrder(null, editedOrder));
		} catch(OrderNotFoundException ex) {
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditSellOrderNullId()throws OrderbookPersistenceException {
		// ARRANGE
		// Create edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("191"), 40, OrderType.SELL);

		// ACT & ASSERT
		try {
			assertNull(service.editSellOrder(null, editedOrder));
		} catch(OrderNotFoundException ex) {
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditBuyOrderNullEditedOrder()throws OrderbookPersistenceException {
		// ARRANGE
		// Add order to DAO
		OrderDto buyOrderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(buyOrderDto);

		// ACT & ASSERT
		try {
			assertNull(service.editBuyOrder("BORD1", null));
		} catch(OrderNotFoundException ex) {
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditSellOrderNullEditedOrder() throws OrderbookPersistenceException{
		// ARRANGE
		// Add order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// ACT & ASSERT
		try {
			assertNull(service.editSellOrder("SORD1", null));
		} catch(OrderNotFoundException ex) {
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditBuyOrderIdNotFound() throws OrderbookPersistenceException{
		// ARRANGE
		// Create edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("191"), 40, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown OrderNotFoundException");
		} catch(OrderNotFoundException ex) {
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditSellOrderIdNotFound() throws OrderbookPersistenceException{
		// ARRANGE
		// Create edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("191"), 40, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown OrderNotFoundException");
		} catch(OrderNotFoundException ex) {
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditBuyOrderIdFoundPriceWellUnder190() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("1"), 40, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundPriceWellUnder190() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("1"), 40, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}


	@Test
	void testEditBuyOrderIdFoundPriceRightUnder190() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("189.99999999"), 40, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundPriceRightUnder190() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("189.99999999"), 40, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditBuyOrderIdFoundPriceRightOver191() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("191.0000001"), 40, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundPriceRightOver191()throws OrderbookPersistenceException {
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("191.0000001"), 40, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditBuyOrderIdFoundPriceWellOver191() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("454534543"), 40, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundPriceWellOver191() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("454534543"), 40, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditBuyOrderIdFoundQuantityWellUnder20() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("190"), -432432, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundQuantityWellUnder20() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("190"), -432432, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditBuyOrderIdFoundQuantityRightUnder20() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("190"), 19, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundQuantityRightUnder20() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("190"), 19, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditBuyOrderIdFoundQuantityRightOver50()throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("190"), 51, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundQuantityRightOver50() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("190"), 51, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}


	@Test
	void testEditBuyOrderIdFoundQuantityWellOver50() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("190"), 432423, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundQuantityWellOver50() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("190"), 432423, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}


	@Test
	void testEditBuyOrderIdFoundPriceExactly190QuantitExactly20() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("190"), 20, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditSellOrderIdFoundPriceExactly190QuantitExactly20() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("190"), 20, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditBuyOrderIdFoundPriceExactly191QuantitExactly50()throws OrderbookPersistenceException {
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("191"), 50, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditSellOrderIdFoundPriceExactly191QuantitExactly50()throws OrderbookPersistenceException {
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("191"), 50, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
			fail("Shouldn't have thrown InvalidOrderException");
		}
	}

	@Test
	void testEditBuyOrderIdFoundSellType() throws OrderbookPersistenceException{
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("BORD1", new BigDecimal("191"), 50, OrderType.SELL);

		// ACT & ASSERT
		try {
			service.editBuyOrder("BORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	@Test
	void testEditSellOrderIdFoundBuyType()throws OrderbookPersistenceException {
		// ARRANGE

		// Add valid order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create invalid edited order
		Order editedOrder = new Order("SORD1", new BigDecimal("191"), 50, OrderType.BUY);

		// ACT & ASSERT
		try {
			service.editSellOrder("SORD1", editedOrder);
			fail("Should have thrown InvalidOrderException");
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		} catch(InvalidOrderException ex) {
		}
	}

	// REMOVE ORDER TESTS

	@Test
	void testRemoveBuyOrderNullId()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			assertNull(service.removeBuyOrder(null));
		} catch(OrderNotFoundException ex) {
		}
	}

	@Test
	void testRemoveSellOrderNullId()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			assertNull(service.removeSellOrder(null));
		} catch(OrderNotFoundException ex) {
		}
	}

	@Test
	void testRemoveBuyOrderIdNotFound()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			service.removeBuyOrder("BORD1");
			fail("Should have thrown OrderNotFoundException");
		} catch(OrderNotFoundException ex) {
		}
	}

	@Test
	void testRemoveSellOrderIdNotFound()throws OrderbookPersistenceException {
		// ACT & ASSERT
		try {
			service.removeSellOrder("SORD1");
			fail("Should have thrown OrderNotFoundException");
		} catch(OrderNotFoundException ex) {
		}
	}

	@Test
	void testRemoveBuyOrderIdFound()throws OrderbookPersistenceException {
		// ARRANGE

		// Add order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.BUY);
		orderDaoStub.addBuyOrder(orderDto);

		// Create equivalent Order to compare
		Order order = new Order("BORD1", new BigDecimal("190"), 30, OrderType.BUY);

		// ACT & ASSERT
		try {
			assertEquals(order, service.removeBuyOrder("BORD1"));
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		}
	}

	@Test
	void testRemoveSellOrderIdFound()throws OrderbookPersistenceException {
		// ARRANGE

		// Add order to DAO
		OrderDto orderDto = new OrderDto(new BigDecimal("190"), 30, OrderType.SELL);
		orderDaoStub.addSellOrder(orderDto);

		// Create equivalent Order to compare
		Order order = new Order("SORD1", new BigDecimal("190"), 30, OrderType.SELL);

		// ACT & ASSERT
		try {
			assertEquals(order, service.removeSellOrder("SORD1"));
		} catch(OrderNotFoundException ex) {
			fail("Shouldn't have thrown OrderNotFoundException");
		}
	}


}
