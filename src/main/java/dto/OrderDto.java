package dto;

import java.math.BigDecimal;

import model.OrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderDto {

    private BigDecimal price;
    private int quantity;
    private OrderType orderType;

    public OrderDto(BigDecimal price, int quantity, OrderType orderType) {
        this.price = price;
        this.quantity = quantity;
        this.orderType = orderType;
    }

    public OrderDto() {
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

	public OrderType getOrderType() {
		return orderType;
	}

	public void setOrderType(OrderType orderType) {
		this.orderType = orderType;
	}
}
