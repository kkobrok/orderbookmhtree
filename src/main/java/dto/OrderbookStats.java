package dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class OrderbookStats {
    private long numberOfSellOrders;
    private long numberOfBuyOrders;
    private long overallSellQuantity;
    private long overallBuyQuantity;
    private BigDecimal averageSellPrice;
    private BigDecimal averageBuyPrice;

    public OrderbookStats(long numberOfSellOrders, long numberOfBuyOrders,
                          long overallSellQuantity, long overallBuyQuantity,
                          BigDecimal averageSellPrice, BigDecimal averageBuyPrice) {
        this.numberOfSellOrders = numberOfSellOrders;
        this.numberOfBuyOrders = numberOfBuyOrders;
        this.overallSellQuantity = overallSellQuantity;
        this.overallBuyQuantity = overallBuyQuantity;
        this.averageSellPrice = averageSellPrice;
        this.averageBuyPrice = averageBuyPrice;
    }

    public OrderbookStats() {
    }

    public long getNumberOfSellOrders() {
        return numberOfSellOrders;
    }

    public void setNumberOfSellOrders(long numberOfSellOrders) {
        this.numberOfSellOrders = numberOfSellOrders;
    }

    public long getNumberOfBuyOrders() {
        return numberOfBuyOrders;
    }

    public void setNumberOfBuyOrders(long numberOfBuyOrders) {
        this.numberOfBuyOrders = numberOfBuyOrders;
    }

    public long getOverallSellQuantity() {
        return overallSellQuantity;
    }

    public void setOverallSellQuantity(long overallSellQuantity) {
        this.overallSellQuantity = overallSellQuantity;
    }

    public long getOverallBuyQuantity() {
        return overallBuyQuantity;
    }

    public void setOverallBuyQuantity(long overallBuyQuantity) {
        this.overallBuyQuantity = overallBuyQuantity;
    }

    public BigDecimal getAverageSellPrice() {
        return averageSellPrice;
    }

    public void setAverageSellPrice(BigDecimal averageSellPrice) {
        this.averageSellPrice = averageSellPrice;
    }

    public BigDecimal getAverageBuyPrice() {
        return averageBuyPrice;
    }

    public void setAverageBuyPrice(BigDecimal averageBuyPrice) {
        this.averageBuyPrice = averageBuyPrice;
    }

    @Override
    public String toString() {
        return "OrderbookStats{" +
                "numberOfSellOrders=" + numberOfSellOrders +
                ", numberOfBuyOrders=" + numberOfBuyOrders +
                ", overallSellQuantity=" + overallSellQuantity +
                ", overallBuyQuantity=" + overallBuyQuantity +
                ", averageSellPrice=" + averageSellPrice +
                ", averageBuyPrice=" + averageBuyPrice +
                '}';
    }
}
