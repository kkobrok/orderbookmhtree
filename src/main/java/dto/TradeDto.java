package dto;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Component
public class TradeDto {
   
    private LocalDateTime executionTime;
    private int quantityFilled;
    private BigDecimal executedPrice;
    private String sellOrderID;
    private String buyOrderID;

    public TradeDto() {
    }


    public TradeDto(LocalDateTime executionTime, int quantityFilled, BigDecimal executedPrice, String sellOrderID, String buyOrderID) {
        this.executionTime = executionTime;
        this.quantityFilled = quantityFilled;
        this.executedPrice = executedPrice;
        this.sellOrderID = sellOrderID;
        this.buyOrderID = buyOrderID;
    }

    public LocalDateTime getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(LocalDateTime executionTime) {
        this.executionTime = executionTime;
    }

    public int getQuantityFilled() {
        return quantityFilled;
    }

    public void setQuantityFilled(int quantityFilled) {
        this.quantityFilled = quantityFilled;
    }

    public BigDecimal getExecutedPrice() {
        return executedPrice;
    }

    public void setExecutedPrice(BigDecimal executedPrice) {
        this.executedPrice = executedPrice;
    }

    public String getSellOrderID() {
        return sellOrderID;
    }

    public void setSellOrderID(String sellOrderID) {
        this.sellOrderID = sellOrderID;
    }

    public String getBuyOrderID() {
        return buyOrderID;
    }

    public void setBuyOrderID(String buyOrderID) {
        this.buyOrderID = buyOrderID;
    }
}
