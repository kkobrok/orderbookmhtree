package daoJdbc;

import model.Order;
import model.OrderType;

public class SQLOrderParser {
	private final static String BUY_ORDERS_TABLE_NAME = "BuyOrder";
	private final static String SELL_ORDERS_TABLE_NAME = "SellOrder";
    private final static String ID_COLUMN_LABEL = "id";

	/** 
	 * Takes an Order and returns an SQL query 
	 * to insert it into the corresponding table
	 * based on its type.
	 * 
	 * @param order buy order to be inserted
	 * @param orderType type of order (buy or sell)
	 * @return the SQL query to insert it into the database, as a String
	 * */
	public String insertOrderQuery(Order order, OrderType orderType) {
		// id column value is null because it is automatically generated (auto increment)
		return String.format("INSERT INTO sql4403710.%s VALUES(NULL, %.2f, %d);",
				orderType.equals(OrderType.BUY)? BUY_ORDERS_TABLE_NAME : SELL_ORDERS_TABLE_NAME,
				order.getPrice(), 
				order.getQuantity());
	}
	
	/** 
	 * Returns an SQL query to retrieve all orders (all columns) 
	 * from the buy or sell orders table based on the type parameter.
	 * @param orderType type of order (buy or sell)
	 * @return SQL query to retrieve all orders in the corresponding table, as a String
	 */
	public String selectAllOrdersQuery(OrderType orderType) {
		return String.format("SELECT * FROM sql4403710.%s;", 
				orderType.equals(OrderType.BUY)? BUY_ORDERS_TABLE_NAME : SELL_ORDERS_TABLE_NAME);
	}
	
	/** 
	 * Takes the id from an Order and returns an SQL query 
	 * to retrieve it (all columns) from the corresponding table 
	 * based on its type.
	 * 
	 * @param id id of the order to be retrieved
	 * @param orderType type of order (buy or sell)
	 * @return SQL query to retrieve the order with that id, as a String
	 */
	public String selectOrderQuery(String id, OrderType orderType) {
		return String.format("SELECT * FROM sql4403710.%s WHERE %s = '%d';", 
				orderType.equals(OrderType.BUY)? BUY_ORDERS_TABLE_NAME : SELL_ORDERS_TABLE_NAME,
				ID_COLUMN_LABEL,
				extractOrderNumber(id));
	}
	
	/** 
	 * Takes the id from an Order and returns an SQL query 
	 * to remove it from the corresponding table based on its type.
	 * 
	 * @param id id of the order to be removed
	 * @param orderType type of order (buy or sell)
	 * @return SQL query to remove the buy order with that id, as a String
	 */
	public String removeOrderQuery(String id, OrderType orderType) {
		return String.format("DELETE FROM sql4403710.%s WHERE %s = '%d';", 
				orderType.equals(OrderType.BUY)? BUY_ORDERS_TABLE_NAME : SELL_ORDERS_TABLE_NAME,
				ID_COLUMN_LABEL,
				extractOrderNumber(id));
	}
	
	/** 
	 * Takes the id from an existing Order and the edited Order 
	 * and returns an SQL query to update it to the edited values on the corresponding table
	 * based on its type.
	 * 
	 * @param id id of the buy order to be updated
	 * @param order edited buy order
	 * @return SQL query to edit the buy order with that id, as a String
	 */
    public String updateOrderQuery(String id, Order order, OrderType orderType) {
    	return String.format(
			"UPDATE sql4403710.%s SET price = %.2f, quantity = %d WHERE %s = '%d';",
				orderType.equals(OrderType.BUY)? BUY_ORDERS_TABLE_NAME : SELL_ORDERS_TABLE_NAME,
				order.getPrice(), 
				order.getQuantity(),
				ID_COLUMN_LABEL,
				extractOrderNumber(id));
    }
    
	/** 
	 * Receives a String order ID with prefix (e.g. "BORD1", "SORD2")
	 * and returns the number part of the ID as an int.
	 * 
	 * @param orderId id of a buy or sell order
	 * @return int for the number inside the id
	 */
    private int extractOrderNumber(String orderId) {
		// Remove the prefix
		String numberSubstring = orderId.substring(OrderType.BUY.getPrefix().length());
        
		// Parse the remaining String to int
		return Integer.parseInt(numberSubstring);
    }
}
