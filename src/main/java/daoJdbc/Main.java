package daoJdbc;

import dto.TradeDto;
import model.Trade;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Main {


    public static void main(String[] args) {
        TradeDaoImplJdbc testDao = new TradeDaoImplJdbc();
        TradeDto testTrade = new TradeDto();
        testTrade.setExecutedPrice(new BigDecimal(900));
        testTrade.setQuantityFilled(30);
        testTrade.setExecutionTime(LocalDateTime.of(2020, 12, 1,6,6,6));
        testTrade.setBuyOrderID("BORD1");
        testTrade.setSellOrderID("SORD1");
//        testDao.addTrade(testTrade);
//        testDao.removeTrade("4");

    }
}