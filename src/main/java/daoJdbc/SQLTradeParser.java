package daoJdbc;

import model.Trade;

public class SQLTradeParser {
	
	/** 
	 * Takes a Trade and returns an SQL query 
	 * to insert it into the database.
	 * @param trade trade to be inserted
	 * @return the SQL query to insert it into the database, as a String
	 * */
    public String insertTradeQuery(Trade trade) {
    	// id column value is null because it is automatically generated (auto increment)
        String query = "INSERT INTO sql4403710.Trades VALUES (NULL,'" + trade.getExecutionTime()
                + "', '" + trade.getQuantityFilled() + "', '"
                + trade.getExecutedPrice() + "', '" + trade.getSellOrderID()
                + "', '" + trade.getBuyOrderID()
                + "');";

        return query;
    }

	/** 
	 * Returns an SQL query to retrieve all trades (all columns) from the database.
	 * @return SQL query to retrieve all trades, as a String
	 */
    public String selectAllTradesQuery() {
        return "SELECT * FROM sql4403710.Trades";
    }

	/** 
	 * Takes the id from a Trade and returns an SQL query 
	 * to retrieve it (all columns) from the database.
	 * 
	 * @param id id of the trade to be retrieved
	 * @return SQL query to retrieve the trade with that id, as a String
	 */
    public String selectTradeQuery(String id) {
        return "SELECT * FROM sql4403710.Trades WHERE id = " + id;
    }

	/** 
	 * Takes the id from a Trade and returns an SQL query 
	 * to remove it from the database.
	 * 
	 * @param id id of the trade to be removed
	 * @return SQL query to remove the trade with that id, as a String
	 */
    public String removeTradeQuery(String id) {
        return "DELETE FROM sql4403710.Trades WHERE id = " + id;
    }

	/** 
	 * Takes the id from an existing Trade and the edited Trade 
	 * and returns an SQL query to update it to the edited values on the database.
	 * 
	 * @param id id of the trade to be updated
	 * @param trade edited trade
	 * @return SQL query to edit the trade with that id, as a String
	 */
    public String updateTradeQuery(String tradeId,Trade trade) {
        String query = "";

        query = "UPDATE sql4403710.Trades SET `executionTime` = " + trade.getExecutionTime()
                + "',`quantityFilled` = '" + trade.getQuantityFilled()
                + "', `executedPrice` ='"+ trade.getExecutedPrice()
                + "', `sellOrderID` ='" + trade.getSellOrderID()
                + "', `buyOrderID` ='" + trade.getBuyOrderID()
                + "' WHERE id = '" + tradeId + "';";
        return query;
    }


}
