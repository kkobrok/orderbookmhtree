package model;

import dto.TradeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Component
public class Trade {

 
    private String tradeID;
    private LocalDateTime executionTime;
    private int quantityFilled;
    private BigDecimal executedPrice;
    private String sellOrderID;
    private String buyOrderID;
    
    public Trade () {
    	
    }

    public String getSellOrderID() {
        return sellOrderID;
    }

    public void setSellOrderID(String sellOrderID) {
        this.sellOrderID = sellOrderID;
    }

    public String getBuyOrderID() {
        return buyOrderID;
    }

    public void setBuyOrderID(String buyOrderID) {
        this.buyOrderID = buyOrderID;
    }

    public Trade(String tradeID, LocalDateTime executionTime,
                 int quantityFilled, BigDecimal executedPrice, String sellOrderID, String buyOrderID) {
        this.tradeID = tradeID;
        this.executionTime = executionTime;
        this.quantityFilled = quantityFilled;
        this.executedPrice = executedPrice;
        this.sellOrderID = sellOrderID;
        this.buyOrderID = buyOrderID;
    }

    public Trade(String tradeID) {
        this.tradeID = tradeID;
    }

    public Trade(String tradeID, TradeDto tradeDto) {
        this.tradeID = tradeID;
        this.executionTime = tradeDto.getExecutionTime();
        this.quantityFilled = tradeDto.getQuantityFilled();
        this.executedPrice = tradeDto.getExecutedPrice();
        this.sellOrderID = tradeDto.getSellOrderID();
        this.buyOrderID = tradeDto.getBuyOrderID();
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trade trade = (Trade) o;
        return quantityFilled == trade.quantityFilled && Objects.equals(tradeID, trade.tradeID) && Objects.equals(executionTime, trade.executionTime) && Objects.equals(executedPrice, trade.executedPrice) && Objects.equals(sellOrderID, trade.sellOrderID) && Objects.equals(buyOrderID, trade.buyOrderID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tradeID, executionTime, quantityFilled, executedPrice, sellOrderID, buyOrderID);
    }


    public String getTradeID() {
        return tradeID;
    }

    public void setTradeID(String tradeID) {
        this.tradeID = tradeID;
    }

    public LocalDateTime getExecutionTime() {
        return executionTime;
    }

    public void setExecutionTime(LocalDateTime executionTime) {
        this.executionTime = executionTime;
    }

    public int getQuantityFilled() {
        return quantityFilled;
    }

    public void setQuantityFilled(int quantityFilled) {
        this.quantityFilled = quantityFilled;
    }

    public BigDecimal getExecutedPrice() {
        return executedPrice;
    }

    public void setExecutedPrice(BigDecimal executedPrice) {
        this.executedPrice = executedPrice;
    }

	@Override
	public String toString() {
		return "Trade [tradeID=" + tradeID + ", executionTime=" + executionTime + ", quantityFilled=" + quantityFilled
				+ ", executedPrice=" + executedPrice + ", sellOrderID=" + sellOrderID + ", buyOrderID=" + buyOrderID
				+ "]";
	}
    
    
}
