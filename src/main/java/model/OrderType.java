package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

public enum OrderType {
	
    BUY("BORD"),
    SELL("SORD");
    
    private String prefix;
    

    OrderType(String prefix) {
        this.prefix = prefix;
    }

    public String getPrefix() {
        return prefix;

    }

    public static OrderType getOrderType( String prefix){
        if (prefix.equals("BORD"))
            return OrderType.BUY;
        if (prefix.equals("SORD"))
            return OrderType.SELL;
        return null;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }



}
