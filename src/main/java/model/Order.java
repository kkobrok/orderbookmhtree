package model;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import dto.OrderDto;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * @author ishar
 *
 */
/**
 * @author ishar
 *
 */
@Component

public class Order {

    private String orderID;
    private BigDecimal price;
    private int quantity;
    private OrderType orderType;

    public Order() {

    }

    public Order(String orderID) {
        this.orderID = orderID;
    }

    public Order(String orderID, BigDecimal price, int quantity, OrderType orderType) {
        this.orderID = orderID;
        this.price = price;
        this.quantity = quantity;
        this.orderType = orderType;
    }

    public Order(String orderID, OrderDto dto) {
    	this.orderID = orderID;
    	this.price = dto.getPrice();
    	this.quantity = dto.getQuantity();
    	this.orderType = dto.getOrderType();
    }

    public OrderType getOrderType() {
        return orderType;
    }

    public void setOrderType(OrderType orderType) {
        this.orderType = orderType;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + Objects.hashCode(this.orderID);
        hash = 53 * hash + Objects.hashCode(this.price);
        hash = 53 * hash + this.quantity;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (this.quantity != other.quantity) {
            return false;
        }
        if (!Objects.equals(this.orderID, other.orderID)) {
            return false;
        }
        if (!Objects.equals(this.price, other.price)) {
            return false;
        }
        return true;
    }

	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", price=" + price + ", quantity=" + quantity + ", orderType=" + orderType
				+ "]";
	}



}
