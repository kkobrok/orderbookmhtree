import controller.OrderbookController;
import exceptions.InvalidOrderException;
import exceptions.OrderNotFoundException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Controller;

public class App {
    public static void main(String[] args) throws InvalidOrderException, OrderNotFoundException {
    	AnnotationConfigApplicationContext appContext = new AnnotationConfigApplicationContext();
        appContext.scan(new String []{"controller","dao","dto","exceptions","model","service","view"}); 
        appContext.refresh();
        OrderbookController controller = appContext.getBean("orderbookController", OrderbookController.class);
        controller.run();
    }
}
