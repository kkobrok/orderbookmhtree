package exceptions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class InvalidOrderException extends Exception {

	public InvalidOrderException () {
		
	}
	
	public InvalidOrderException(String message, Throwable cause) {
		super(message, cause);
	}

	public InvalidOrderException(String message) {
		super(message);
	}
}
