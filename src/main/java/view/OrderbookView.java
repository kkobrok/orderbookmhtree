package view;

import dto.OrderDto;
import dto.OrderbookStats;
import model.Order;
import model.OrderType;
import model.Trade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderbookView {
	@Autowired
    UserIO io;

    public void importDataBanner(){
        io.print("--- Importing Data ---");
    }
    public void exportDataBanner(){
        io.print("--- Exporting Data ---");
    }
    public void displayOrderBookBanner(){
        io.print("--- Displaying Order Book ---");
    }
    public void displayOrderBookStatsBanner(){
        io.print("--- Displaying Order Book Stats ---");
    }
    public void matchBanner(){
        io.print("--- Match ---");
    }
    public void matchAllBanner(){
        io.print("--- Match All ---");
    }
    public void selectedTradeBanner(){
        io.print("--- Selected Trade ---");
    }
    public void allTradeBanner(){
        io.print("--- All Trades ---");
    }
    public void addBuyOrderBanner(){
        io.print("--- Adding Buy Order ---");
    }
    public void addSellOrderBanner(){
        io.print("--- Adding Sell Order ---");
    }
    public void editBuyOrderBanner(){
        io.print("--- Editing Buy Order ---");
    }
    public void editSellOrderBanner(){
        io.print("--- Editing Sell Order ---");
    }
    public void removingBuyOrderBanner(){
        io.print("--- Removing Buy Order ---");
    }
    public void removingSellOrderBanner(){
        io.print("--- Removing Sell Order ---");
    }
    public void taskCompletedBanner(){
        io.print("--- Task Completed ---");
    }
    public void print(String s){
        io.print(s);
    }


    public int printMenuAndGetSelection() {
        io.print("Main Menu");
        io.print("1. Display Order Book");
        io.print("2. Display Order Book Stats");
        io.print("3. Match");
        io.print("4. Match All");
        io.print("5. View Trade");
        io.print("6. View All Trades");
        io.print("7. Add Buy Order");
        io.print("8. Add Sell Order");
        io.print("9. Edit Buy Order");
        io.print("10. Edit Sell Order");
        io.print("11. Remove Buy Order");
        io.print("12. Remove Sell Order");

        io.print("13. Exit");

        return io.readInt("Please select from the above choices.", 1, 13);
    }
    
    private List<List<Order>> paginateOrderList(List<Order> orderList) {
    	int ordersPerPage = 20;
    	
        List<List<Order>> ordersInPages = new ArrayList<>();

        int totalPages = orderList.size() < ordersPerPage? 1 : (int) Math.ceil((double)orderList.size() / (double)ordersPerPage);
        int i = 0;
        
        for(int currentPageIndex = 0; currentPageIndex < totalPages; currentPageIndex++) {
        	ordersInPages.add(new ArrayList<>());
        	
        	// Keep adding orders while there are still left and page is not complete
        	int ordersInThisPage = 0;
        	while (i < orderList.size() && ordersInThisPage < ordersPerPage) {
        		ordersInPages.get(currentPageIndex).add(orderList.get(i));
        		i++;
        		ordersInThisPage++;
        	}
        }
        
        return ordersInPages;
    }

    public void display(List<Order> buyOrderList, List<Order> sellOrderList){
        // Order buy orders from highest to lowest price; sell orders the reverse
    	buyOrderList.sort((Order o1, Order o2) -> o2.getPrice().compareTo(o1.getPrice()));
        sellOrderList.sort((Order o1, Order o2) -> o1.getPrice().compareTo(o2.getPrice()));
        
        // Put orders in groups of 'ordersPerPage' inside an array of arrays
        List<List<Order>> buyListArray = paginateOrderList(buyOrderList);
        List<List<Order>> sellListArray = paginateOrderList(sellOrderList);
 
        // Read pagination commands on loop until they enter quit
        int pageInView = 0;
    	int maxBuyPageIndex = buyListArray.size() - 1;
    	int maxSellPageIndex = sellListArray.size() - 1;
    	int maxPageIndex = Math.max(maxBuyPageIndex, maxSellPageIndex);
    	
        boolean viewingOrderbook = true;
        
        while (viewingOrderbook) {
        	
        	print("\n\n************* Page " + (pageInView + 1) + "*************");
        	print("BUY                                                                  SELL\n");

    		// Print orders inside the current page
    		int maxBuyOrderIndex = pageInView <= maxBuyPageIndex? 
    				buyListArray.get(pageInView).size() - 1 : -1;
    		
    		int maxSellOrderIndex = pageInView <= maxSellPageIndex? 
    				sellListArray.get(pageInView).size() - 1 : -1;
    		
    		int maxOrderIndex = Math.max(maxBuyOrderIndex, maxSellOrderIndex);	
    		
        	for(int j = 0; j <= maxOrderIndex; j++) {

        		// If there is 1 of each, print both
        		if (j <= maxBuyOrderIndex && j <= maxSellOrderIndex) {
        			System.out.println(buyListArray.get(pageInView).get(j) + "     " + sellListArray.get(pageInView).get(j));
        		}
        	    // If there are only sell orders remaining, just print right side
        		else if (j <= maxSellOrderIndex) {
        			System.out.println("                                                                     " + sellListArray.get(pageInView).get(j));
        		} 
        		// If there are only buy orders remaining, just print left side
        		else if (j <= maxBuyOrderIndex){
        			System.out.println(buyListArray.get(pageInView).get(j));
        		}
        	}
        	
        	// Show pagination instructions
        	String input = io.readString("Enter 'N' to see next page. \n"
        			+ "Enter 'P' to see previous page. \n"
        			+ "Enter 'Q' to quit viewing the orderbook");
        	
        	// See which page to show next
        	switch(input) {
        		case "N" : 	{
        			if (pageInView == maxPageIndex) {
						viewingOrderbook = false;
					}
        			pageInView++; 
					
					break;
        		}
        		case "P" : {
    				if (pageInView == 0) {
						viewingOrderbook = false;
					}
        			pageInView--; 
	
					break;		
        		}
        		case "Q" : {
        			viewingOrderbook = false;
        			break;
        		}
        		default:
        			print("Unrecognized command");
        	}
    	} 
    }
    
    public void displayStats(OrderbookStats orderbookStats){
        System.out.println(orderbookStats);
    }

    public OrderDto readBuyOrder(){
        BigDecimal price = io.readBigDecimal("Please enter the price", new BigDecimal("190.00"), new BigDecimal("191.00"));
        int quantity = io.readInt("Please enter a quantity between 20 and 50", 20, 50);
        OrderType orderType = io.buyOrderType();
        return new OrderDto(price, quantity, orderType);
    }

    public OrderDto readSellOrder(){
        BigDecimal price = io.readBigDecimal("Please enter the price", new BigDecimal("190"), new BigDecimal("191"));
        int quantity = io.readInt("Please enter a quantity between 20 and 50", 20, 50);
        OrderType orderType = io.sellOrderType();
        return new OrderDto(price, quantity, orderType);
    }
    
    public void printOrderBookStats(OrderbookStats obs) {
    	System.out.println(obs);
    }

    public void viewAllTrades(List<Trade> tradesList){
        tradesList.forEach(System.out::println);
    }

    public void printTrade(Trade trade){
        System.out.println(trade);
    }
    
    public void printOrder(Order order){
        System.out.println(order);
    }


    public String getTradeId(){
        return io.readString("Please enter Trade ID");
    }

    public String getOrderId(){
        return io.readString("Please enter Order ID");
    }

    public void printMatchAll(List<Trade> trades){
        trades.forEach(System.out::println);
    }

    public void printMatch(Trade trade){
        System.out.println(trade);
    }

    public BigDecimal getPrice(){
        return io.readBigDecimal("Please enter the price", new BigDecimal("190"), new BigDecimal("191"));
    }

    public int getQuantity(){
        return io.readInt("Please enter a quantity between 20 and 50", 20, 50);
    }

    public void displayErrorMessage(String errorMsg) {
        io.print("=== ERROR ===");
        io.print(errorMsg);
    }
}
