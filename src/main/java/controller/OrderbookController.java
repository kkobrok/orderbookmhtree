package controller;

//import com.sun.tools.corba.se.idl.constExpr.Or;
import dto.OrderDto;
import dto.OrderbookStats;
import exceptions.InvalidOrderException;
import exceptions.OrderNotFoundException;
import exceptions.OrderbookPersistenceException;
import model.Order;
import model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import service.OrderbookService;
import view.OrderbookView;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class OrderbookController {

    private final OrderbookView view;
    private final OrderbookService service;

    @Autowired
    public OrderbookController(OrderbookView view, OrderbookService service) {
        this.view = view;
        this.service = service;

    }

    public void run() throws InvalidOrderException, OrderNotFoundException {

        boolean flag = true;
        try {
        while (flag) {
            int menuSelection = view.printMenuAndGetSelection();
            switch (menuSelection) {
                case 1:
                	display();
                    break;
                case 2:
                	displayStats();
                    break;
                case 3:
                	match();
                    break;
                case 4:
                	matchAll();
                    break;
                case 5:
                	viewTrade();
                    break;
                case 6:
                	viewAllTrades();
                    break;
                case 7:
                	addBuyOrder();
                    break;
                case 8:
                	addSellOrder();
                    break;
                case 9:
                	editBuyOrder();
                    break;
                case 10:
                	editSellOrder();
                    break;
                case 11:
                	removeBuyOrder();
                    break;
                case 12:
                	removeSellOrder();
                    break;
                case 13:
                    System.out.println("Exiting");
                    flag = false;
                    break;
                default:
                    System.out.println("Incorrect Entery");
            }
        }
    }catch (OrderbookPersistenceException | InvalidOrderException |
    OrderNotFoundException e){
        view.displayErrorMessage(e.getMessage());
    	}
    }
    

    public void display() throws OrderbookPersistenceException {
        view.displayOrderBookBanner();
        List<Order> buyList = service.getAllBuyOrders();
        List<Order> sellList = service.getAllSellOrders();
        view.display(buyList, sellList);
        view.taskCompletedBanner();
    }

    public void displayStats() throws OrderbookPersistenceException {
        view.displayOrderBookStatsBanner();
        OrderbookStats obs = service.getOrderbookStats();
        view.printOrderBookStats(obs);
        view.taskCompletedBanner();
    }

    public void match() throws OrderbookPersistenceException {
        view.matchBanner();
        view.printMatch(service.match());
        view.taskCompletedBanner();
    }

    public void matchAll() throws OrderbookPersistenceException {
        view.matchAllBanner();
        List<Trade> allTrades = service.matchAll();
        view.printMatchAll(allTrades);
        view.taskCompletedBanner();
    }

    public void viewTrade() throws OrderbookPersistenceException {
        view.selectedTradeBanner();
        String tradeId = view.getTradeId();
        Trade returnedTrade = service.getTradeById(tradeId);
        if(returnedTrade == null) {
        	view.print("Trade not found");
        	return;
        }
        view.printTrade(returnedTrade);
        view.taskCompletedBanner();
    }

    public void viewAllTrades() throws OrderbookPersistenceException {
        view.allTradeBanner();
        List<Trade> tradesList = service.getAllTradesSortedByExecutionTime();
        view.viewAllTrades(tradesList);
        view.taskCompletedBanner();
    }

    public void addBuyOrder() throws InvalidOrderException, OrderbookPersistenceException {
        view.addBuyOrderBanner();
        OrderDto buyOrder = view.readBuyOrder();
        Order order = service.addBuyOrder(buyOrder);
        view.printOrder(order);
        view.taskCompletedBanner();
    }

    public void addSellOrder() throws InvalidOrderException, OrderbookPersistenceException {
        view.addSellOrderBanner();
        OrderDto sellOrder = view.readSellOrder();
        Order order = service.addSellOrder(sellOrder);
        view.printOrder(order);
        view.taskCompletedBanner();
    }

    public void editBuyOrder() throws InvalidOrderException, OrderNotFoundException, OrderbookPersistenceException {
        view.editBuyOrderBanner();
        String orderId = view.getOrderId();
        Order buyOrder = service.getBuyOrderById(orderId);
        if (buyOrder == null){
        	view.print("Error: does not exist");
        	return;
        }
        view.print("Old Order");
    	view.printOrder(buyOrder);
        BigDecimal price = view.getPrice();
        int quantity = view.getQuantity();
        buyOrder.setPrice(price);
        buyOrder.setQuantity(quantity);
        Order replacedOrder = service.editBuyOrder(orderId, buyOrder);
        if (replacedOrder == null){
        	view.print("Error couldnt edit");
        } else {
        	view.print("Updated Order");
        	view.printOrder(replacedOrder);
        	view.taskCompletedBanner();
        }
    }

    public void editSellOrder() throws InvalidOrderException, OrderNotFoundException, OrderbookPersistenceException {
        view.editSellOrderBanner();
        String orderId = view.getOrderId();
        Order sellOrder = service.getSellOrderById(orderId);
        if (sellOrder == null){
        	view.print("Error: does not exist");
        	return;
        }
        view.print("Old Order");
    	view.printOrder(sellOrder);
        
    	BigDecimal price = view.getPrice();
        int quantity = view.getQuantity();
        sellOrder.setPrice(price);
        sellOrder.setQuantity(quantity);
        
        Order replacedOrder = service.editSellOrder(orderId, sellOrder);
        if (replacedOrder == null){
        	view.print("Error couldnt edit");
        } else {
        	view.print("Updated Order");
        	view.printOrder(replacedOrder);
        	view.taskCompletedBanner();
        }
    }

    public void removeBuyOrder() throws OrderNotFoundException, OrderbookPersistenceException {
        view.removingBuyOrderBanner();
        String buyId = view.getOrderId();
        if (service.getBuyOrderById(buyId) == null) {
        	view.print("Error order does not exist");
        	return;
        } 
        Order removeOrder = service.removeBuyOrder(buyId);
        if (removeOrder == null) {
        	view.print("Error - Could not remove");
        } else {
        	view.printOrder(removeOrder);
        	view.taskCompletedBanner();
        }
    }

    public void removeSellOrder() throws OrderNotFoundException, OrderbookPersistenceException {
    	view.removingBuyOrderBanner();
        String sellId = view.getOrderId();
        if (service.getSellOrderById(sellId) == null) {
        	view.print("Error order does not exist");
        	return;
        } 
        Order removeOrder = service.removeSellOrder(sellId);
        if (removeOrder == null) {
        	view.print("Error - Could not remove");
        } else {
        	view.printOrder(removeOrder);
        	view.taskCompletedBanner();
        }

    }
}
