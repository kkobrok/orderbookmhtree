package dao;

import java.util.List;

import dto.OrderDto;
import exceptions.OrderbookPersistenceException;
import model.Order;

public interface OrderDao {

    String addBuyOrder(OrderDto buyOrderDto) throws OrderbookPersistenceException;
    String addSellOrder(OrderDto sellOrderDto) throws OrderbookPersistenceException;
    Order removeBuyOrder(String buyOrderId) throws OrderbookPersistenceException;
    Order removeSellOrder(String sellOrderId) throws OrderbookPersistenceException;
    Order editBuyOrder(String buyOrderId, Order buyOrder) throws OrderbookPersistenceException;
    Order editSellOrder(String sellOrderId, Order sellOrder) throws OrderbookPersistenceException;
    List<Order> getAllBuyOrders() throws OrderbookPersistenceException;
    List<Order> getAllSellOrders() throws OrderbookPersistenceException;
    Order getBuyOrder(String buyOrderId) throws OrderbookPersistenceException;
    Order getSellOrder(String sellOrderId) throws OrderbookPersistenceException;
}
