package dao;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

import dto.OrderDto;
import model.Order;
import model.OrderType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderDaoImpl implements OrderDao {

    public  String ORDER_FILE;

    public static final String DELIMITER = "::";
    
    private Map<String, Order> buyOrders;
    private Map<String, Order> sellOrders;
    private Comparator<String> orderIdComparator;
    
    public OrderDaoImpl() {
    	this("src/main/orders.txt");
    }

    public OrderDaoImpl(String fileName) {
    	/* 
    	 * Orders IDs are numbers with a prefix ("BORD1", "BORD2", "BORD3"...), 
    	 * so we order IDs first by length (ascending), 
    	 * then by alphabetical order (ascending),
    	 * to avoid the default ordering "BORD1", "BORD10", "BORD2", "BORD3"... 
    	 * (only by alphabetical order) 
    	 */
        orderIdComparator = Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder());
        buyOrders = new TreeMap<>(orderIdComparator);
        sellOrders = new TreeMap<>(orderIdComparator);
        
        // Load orders from database
        ORDER_FILE = fileName;
        loadOrders();
    }

    @Override
    public String addBuyOrder(OrderDto buyOrderDto) {
        if (buyOrderDto == null) return null;
        
        // Create order with new ID
        Order buyOrder = new Order(getFreeBuyOrderId());
        
        // Copy DTO fields to new order
        buyOrder.setOrderType(buyOrderDto.getOrderType());
        buyOrder.setPrice(buyOrderDto.getPrice());
        buyOrder.setQuantity(buyOrderDto.getQuantity());
        
        // Add new order to collection
        buyOrders.put(buyOrder.getOrderID(), buyOrder);
        
        // Update on database
        writeOrders();
        
        // Return assigned ID
        return buyOrder.getOrderID();
    }

    @Override
    public String addSellOrder(OrderDto sellOrderDto) {
        if (sellOrderDto == null) return null;
        
        // Create order with new ID
        Order sellOrder = new Order(getFreeSellOrderId());
        
        // Copy DTO fields to new order
        sellOrder.setOrderType(sellOrderDto.getOrderType());
        sellOrder.setPrice(sellOrderDto.getPrice());
        sellOrder.setQuantity(sellOrderDto.getQuantity());
        
        // Add new order to collection
        sellOrders.put(sellOrder.getOrderID(), sellOrder);
        
        // Update on database
        writeOrders();
        
        // Return assigned ID
        return sellOrder.getOrderID();
    }

    @Override
    public Order removeBuyOrder(String buyOrderId) {
        if (buyOrderId == null) return null;
        
        Order removedOrder = buyOrders.remove(buyOrderId);
        
        // Update on database
        writeOrders();
        
        return removedOrder;
    }

    @Override
    public Order removeSellOrder(String sellOrderId) {
        if (sellOrderId == null) return null;
        
        Order removedOrder = sellOrders.remove(sellOrderId);
        
        // Update on database
        writeOrders();
        
        return removedOrder;
    }

    @Override
    public Order editBuyOrder(String buyOrderId, Order buyOrder) {
        if (buyOrderId == null || buyOrder == null) return null;
        
        Order updatedOrder = buyOrders.put(buyOrderId, buyOrder);
        
        // Update on database
        writeOrders();
        
        return updatedOrder;
    }

    @Override
    public Order editSellOrder(String sellOrderId, Order sellOrder) {
        if (sellOrderId == null || sellOrder == null) return null;
        
        Order updatedOrder =  sellOrders.put(sellOrderId, sellOrder);
        
        // Update on database
        writeOrders();
        
        return updatedOrder;
    }

    @Override
    public List<Order> getAllBuyOrders() {
        return new ArrayList<>(buyOrders.values());
    }

    @Override
    public List<Order> getAllSellOrders() {
        return new ArrayList<>(sellOrders.values());
    }

    private String getFreeBuyOrderId() {
        if (buyOrders.isEmpty()) {
            return OrderType.BUY.getPrefix() + 1;
        }
        
        // Initialize TreeSet with comparator, to get the correct last ID
        TreeSet<String> takenIds = new TreeSet<>(orderIdComparator);
        takenIds.addAll(buyOrders.keySet());
        
        String lastId = takenIds.last();
        // Remove alphabetic prefix
        String digitPart = lastId.substring(OrderType.BUY.getPrefix().length()); 
        int digit = Integer.parseInt(digitPart);
        return OrderType.BUY.getPrefix() + (++digit);
    }
    
    private String getFreeSellOrderId() {
        if (sellOrders.isEmpty()) {
            return OrderType.SELL.getPrefix() + 1;
        }
        
        // Initialize TreeSet with comparator, to get the correct last ID
        TreeSet<String> takenIds = new TreeSet<>(orderIdComparator);
        takenIds.addAll(sellOrders.keySet());
        
        String lastId = takenIds.last();
        // Remove alphabetic prefix
        String digitPart = lastId.substring(OrderType.SELL.getPrefix().length());
        int digit = Integer.parseInt(digitPart);
        return OrderType.SELL.getPrefix() + (++digit);
    }

    @Override
    public Order getBuyOrder(String buyOrderId) {
        return buyOrders.get(buyOrderId);
    }

    @Override
    public Order getSellOrder(String sellOrderId) {
        return sellOrders.get(sellOrderId);
    }


    // order persistence ----------------------------------

    private Order unmarshallOrder(String orderAsText) {
        String[] orderTokens = orderAsText.split(DELIMITER);
        String OrderID = orderTokens[0];
        Order orderFromFile = new Order(OrderID);
        orderFromFile.setPrice(new BigDecimal(orderTokens[1]));
        orderFromFile.setQuantity(Integer.parseInt(orderTokens[2]));
        orderFromFile.setOrderType(OrderType.valueOf(orderTokens[3]));
        return orderFromFile;
    }


    private void loadOrders() {
        Scanner scanner = null;

        try {
            // Create Scanner for reading the file
            scanner = new Scanner(new BufferedReader(new FileReader(ORDER_FILE)));
        } catch (FileNotFoundException e) {
            System.out.println("-_- Could not load data into memory.");
        }
        String currentLine;
        Order currentOrder;
        buyOrders.clear();
        sellOrders.clear();
        while (true) {
            assert scanner != null;
            if (!scanner.hasNextLine()) break;

            currentLine = scanner.nextLine();

            currentOrder = unmarshallOrder(currentLine);

            if(currentOrder.getOrderType().getPrefix().equals("BORD")){
                buyOrders.put(currentOrder.getOrderID(), currentOrder);
            } else {
                sellOrders.put(currentOrder.getOrderID(), currentOrder);
            }
        }
        scanner.close();
    }

    private String marshallOrder (Order aOrder) {

        String orderAsText = aOrder.getOrderID() + DELIMITER;
        orderAsText += aOrder.getPrice() + DELIMITER;
        orderAsText += aOrder.getQuantity() + DELIMITER;
        orderAsText += aOrder.getOrderType();

        return orderAsText;
    }

    private void writeOrders() {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(ORDER_FILE));
        } catch (IOException e) {
            System.out.println("Could not save order data");
        }

        String orderAsText;
        List<Order> buyOrderList = this.getAllBuyOrders();
        for (Order currentOrder : buyOrderList) {
            orderAsText = marshallOrder(currentOrder);
            assert out != null;
            out.println(orderAsText);
            out.flush();
        }
        List<Order> sellOrderList = this.getAllSellOrders();
        for (Order currentOrder : sellOrderList) {
            orderAsText = marshallOrder(currentOrder);
            assert out != null;
            out.println(orderAsText);
            out.flush();
        }

        assert out != null;
        out.close();
    }
}

