package dao;
import dto.TradeDto;
import model.Order;
import model.Trade;

import java.util.*;

import model.Order;
import model.Trade;
import org.springframework.stereotype.Component;

import java.io.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Component
public class TradeDaoImpl implements TradeDao{
    public final String ORDER_FILE;
    private Comparator<String> tradeIdComparator;
    public static final String DELIMITER = "::";

    private Map<String, Trade> trades = new TreeMap<>();

    public TradeDaoImpl() {
    	/* 
    	 * Trade IDs are numbers as Strings, 
    	 * so we order IDs first by length (descending), 
    	 * then by alphabetical order (descending),
    	 * to avoid the default ordering "1", "10", "2", "3"... 
    	 * (only by alphabetical order) 
    	 */
        tradeIdComparator = Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder());

        ORDER_FILE = "src/main/trades.txt";
        loadTrades();
    }

    public TradeDaoImpl(String fileName) {
        tradeIdComparator = Comparator.comparingInt(String::length).thenComparing(Comparator.naturalOrder());


        ORDER_FILE = fileName;
        loadTrades();
    }



    @Override
    public String addTrade(TradeDto tradeDto) {
    	
        if (tradeDto == null) return null;
        String freeTradeId = getFreeTradeId();

        Trade trade = new Trade(freeTradeId, tradeDto.getExecutionTime(),
                tradeDto.getQuantityFilled(), tradeDto.getExecutedPrice()
                , tradeDto.getSellOrderID(), tradeDto.getBuyOrderID());
        trades.put(freeTradeId, trade);
        writeTrades();
        return freeTradeId;
    }


    @Override
    public Trade removeTrade(String tradeRemoveId) {
        Trade remove = trades.remove(tradeRemoveId);
        writeTrades();
        return remove;
    }

    @Override
    public Trade editTrade(String tradeEditedId, Trade tradeEdited) {

        Trade replace = trades.replace(tradeEditedId, tradeEdited);
        writeTrades();
        return replace;
    }

    @Override
    public List<Trade> getAllTrades() {
        return new ArrayList<>(trades.values());
    }

    @Override
    public Trade getTradeById(String tradeId) {
        return trades.get(tradeId);
    }

    private String getFreeTradeId() {
        if (trades.isEmpty()) {
            return "1";
        }
        TreeSet<String> takenIds = new TreeSet<>(tradeIdComparator);
        takenIds.addAll(trades.keySet());
        String lastId = takenIds.last();
        int digit = Integer.parseInt(lastId);
        digit++;
        return String.valueOf(digit);
    }

    private Trade unmarshallTrade(String tradesAsText) {

        String[] tradesTokens = tradesAsText.split(DELIMITER);
        String tradeID = tradesTokens[0];
        Trade tradesFromFile = new Trade(tradeID);
        tradesFromFile.setExecutionTime(LocalDateTime.parse(tradesTokens[1]));
        tradesFromFile.setQuantityFilled(Integer.parseInt(tradesTokens[2]));
        tradesFromFile.setExecutedPrice(new BigDecimal(tradesTokens[3]));
        return tradesFromFile;

    }


    private void loadTrades() {
        Scanner scanner = null;

        try {
            scanner = new Scanner(new BufferedReader(new FileReader(ORDER_FILE)));
        } catch (FileNotFoundException e) {
            System.out.println("-_- Could not load data into memory.");
        }
        String currentLine;
        Trade currentTrade;

        while (true) {
            assert scanner != null;
            if (!scanner.hasNextLine()) break;
            currentLine = scanner.nextLine();
            currentTrade = unmarshallTrade(currentLine);
            trades.put(currentTrade.getTradeID(), currentTrade);

        }
        scanner.close();
    }

    private String marshallTrades (Trade aTrade) {

        String tradesAsText = aTrade.getTradeID() + DELIMITER;
        tradesAsText += aTrade.getExecutionTime() + DELIMITER;
        tradesAsText += aTrade.getQuantityFilled() + DELIMITER;
        tradesAsText += aTrade.getExecutedPrice();

        return tradesAsText;
    }

    private void writeTrades() {

        PrintWriter out = null;

        try {
            out = new PrintWriter(new FileWriter(ORDER_FILE));
        } catch (IOException e) {
            System.out.println("Could not save trades data");
        }

        String tradeAsText;
        List<Trade> tradesList = this.getAllTrades();
        for (Trade currentrade : tradesList) {
            tradeAsText = marshallTrades(currentrade);
            assert out != null;
            out.println(tradeAsText);
            out.flush();
        }

        assert out != null;
        out.close();
    }
}
