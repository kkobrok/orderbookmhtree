package dao;

import dto.TradeDto;
import exceptions.OrderbookPersistenceException;
import model.Trade;

import java.util.List;

public interface TradeDao {
    String addTrade(TradeDto tradeDto) throws OrderbookPersistenceException;
    Trade removeTrade(String tradeId) throws OrderbookPersistenceException;
    Trade editTrade(String tradeId, Trade trade) throws OrderbookPersistenceException;
    List<Trade> getAllTrades() throws OrderbookPersistenceException;
    Trade getTradeById(String tradeId) throws OrderbookPersistenceException;


}
