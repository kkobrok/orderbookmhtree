package service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import dao.OrderDao;
import dao.TradeDao;
import dto.OrderDto;
import dto.OrderbookStats;
import dto.TradeDto;
import exceptions.InvalidOrderException;
import exceptions.OrderNotFoundException;
import exceptions.OrderbookPersistenceException;
import model.Order;
import model.OrderType;
import model.Trade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderbookServiceImpl implements OrderbookService {

	private OrderDao orderDao;
	private TradeDao tradeDao;
	
	// A bid-ask spread is the amount by which the ask price exceeds the bid price for an asset
	private final static BigDecimal MINIMUM_BID_ASK_SPREAD = new BigDecimal("0.1").negate();
	private final static BigDecimal MIN_ORDER_PRICE = new BigDecimal("190.00");
	private final static BigDecimal MAX_ORDER_PRICE = new BigDecimal("191.00");
	private final static int MIN_ORDER_QUANTITY = 20;
	private final static int MAX_ORDER_QUANTITY = 50;

	private OrderbookServiceImpl() {
	}
	
	@Autowired
	public OrderbookServiceImpl(OrderDao orderDao, TradeDao tradeDao) throws OrderbookPersistenceException {
		this(orderDao, tradeDao, 1000);
	}
	
	public OrderbookServiceImpl(OrderDao orderDao, TradeDao tradeDao, int numberOfRandomOrders) throws OrderbookPersistenceException {
		this.orderDao = orderDao;
		this.tradeDao = tradeDao;
		if (orderbookIsEmpty()) {
			populateDatabaseWithRandomOrders(numberOfRandomOrders);
		}
	}


	private void populateDatabaseWithRandomOrders(int numberOfRandomOrders) throws OrderbookPersistenceException {

		Random rng = new Random();

		OrderDto currentDto;
		BigDecimal randomBigDecimal, randomPrice;
		int randomQuantity;

		// Add 1000 random orders of each type
		for(int i = 0; i < numberOfRandomOrders; i++) {

			// Add buy order with random price and quantity
			randomBigDecimal = new BigDecimal(rng.nextDouble());
			randomBigDecimal = randomBigDecimal.setScale(2, RoundingMode.HALF_EVEN);

			randomPrice = MIN_ORDER_PRICE.add(randomBigDecimal.multiply(MAX_ORDER_PRICE.subtract(MIN_ORDER_PRICE)));
			randomPrice = randomPrice.setScale(2, RoundingMode.HALF_EVEN);
			randomQuantity = MIN_ORDER_QUANTITY + rng.nextInt(MAX_ORDER_QUANTITY - MIN_ORDER_QUANTITY + 1);

			currentDto = new OrderDto(randomPrice, randomQuantity, OrderType.BUY);
			orderDao.addBuyOrder(currentDto);

			// Add sell order with random price and quantity
			randomBigDecimal = new BigDecimal(rng.nextDouble());
			randomBigDecimal = randomBigDecimal.setScale(2, RoundingMode.HALF_EVEN);

			randomPrice = MIN_ORDER_PRICE.add(randomBigDecimal.multiply(MAX_ORDER_PRICE.subtract(MIN_ORDER_PRICE)));
			randomPrice = randomPrice.setScale(2, RoundingMode.HALF_EVEN);
			randomQuantity = MIN_ORDER_QUANTITY + rng.nextInt(MAX_ORDER_QUANTITY - MIN_ORDER_QUANTITY + 1);

			currentDto = new OrderDto(randomPrice, randomQuantity, OrderType.SELL);
			orderDao.addSellOrder(currentDto);
		}
	}


	@Override
	public OrderbookStats getOrderbookStats() throws OrderbookPersistenceException {

		// CALCULATE STATS

		// Calculate total number of active sell orders
	    long numberOfSellOrders = getAllSellOrders().size();

	    // Calculate total number of active buy orders
	    long numberOfBuyOrders = getAllBuyOrders().size();

	    // Calculate total quantity of active sell orders
	    long overallSellQuantity = getAllSellOrders()
	    		.stream()
	    		.mapToInt(Order::getQuantity)
	    		.reduce(0, (subtotal, element) -> subtotal + element);

	    // Calculate total quantity of active buy orders
	    long overallBuyQuantity = getAllBuyOrders()
	    		.stream()
	    		.mapToInt(Order::getQuantity)
	    		.reduce(0, (subtotal, element) -> subtotal + element);

	    // Calculate average ask price of active sell orders
	    BigDecimal[] totalWithCount = getAllSellOrders()
	    		.stream()
	    		.map(Order::getPrice)
		        .map(bd -> new BigDecimal[]{bd, BigDecimal.ONE})
		        .reduce((a, b) -> new BigDecimal[]{a[0].add(b[0]), a[1].add(BigDecimal.ONE)})
		        // If there are no orders, total = {0} and count = 1 to avoid dividing by zero
		        .orElse(new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ONE});

	    BigDecimal averageSellPrice = totalWithCount[0].divide(totalWithCount[1], RoundingMode.HALF_EVEN);

	    // Calculate average bid price of active buy orders
	    BigDecimal[] totalWithCount2 = getAllBuyOrders()
	    		.stream()
	    		.map(Order::getPrice)
		        .map(bd -> new BigDecimal[]{bd, BigDecimal.ONE})
		        .reduce((a, b) -> new BigDecimal[]{a[0].add(b[0]), a[1].add(BigDecimal.ONE)})
		        .orElse(new BigDecimal[]{BigDecimal.ZERO, BigDecimal.ONE});

	    BigDecimal averageBuyPrice = totalWithCount2[0].divide(totalWithCount2[1], RoundingMode.HALF_EVEN);

	    // Return an OrderbookStats DTO
		return new OrderbookStats(numberOfSellOrders, numberOfBuyOrders, overallSellQuantity, overallBuyQuantity, averageSellPrice, averageBuyPrice);
	}

	@Override
	public List<Order> getAllBuyOrders() throws OrderbookPersistenceException {
		return orderDao.getAllBuyOrders();
	}

	@Override
	public List<Order> getAllSellOrders() throws OrderbookPersistenceException {
		return orderDao.getAllSellOrders();
	}

	@Override
	public Trade match() throws OrderbookPersistenceException {
		
		// Check there's at least one order of each type
		if (orderbookIsEmpty()) {
			return null;
		}
		
		// Get top buy order (highest bid price)
		Order topBuyOrder = orderDao.getAllBuyOrders()
				.stream()
				.max( (Order o1, Order o2)-> o1.getPrice().compareTo(o2.getPrice()) )
				.orElse(null);
		
		// Get top sell order (lowest ask price)
		Order topSellOrder = orderDao.getAllSellOrders()
				.stream()
				.min( (Order o1, Order o2)-> o1.getPrice().compareTo(o2.getPrice()) )
				.orElse(null);

		// Check spread is not below minimum spread
		if (!bidPriceMeetsAskPrice(topBuyOrder, topSellOrder)) {
			return null;
		}
		
		// Save Trade
		LocalDateTime executionTime = LocalDateTime.now();
		int quantityFilled = Math.min(topBuyOrder.getQuantity(), topSellOrder.getQuantity());
		BigDecimal executedPrice = topSellOrder.getPrice();
		String sellOrderID = topSellOrder.getOrderID();
		String buyOrderID = topBuyOrder.getOrderID();
		
		TradeDto tradeDto = new TradeDto(executionTime, quantityFilled, executedPrice, sellOrderID, buyOrderID);
		String tradeId = tradeDao.addTrade(tradeDto);
		
		// Fill buy order
		if (topBuyOrder.getQuantity() == tradeDto.getQuantityFilled()) {
			fillFullBuyOrder(topBuyOrder.getOrderID());
		} else if (topBuyOrder.getQuantity() > tradeDto.getQuantityFilled()){
			fillPartialBuyOrder(topBuyOrder.getOrderID(), tradeDto.getQuantityFilled());
		}
		
		// Fill sell order
		if (topSellOrder.getQuantity() == tradeDto.getQuantityFilled()) {
			fillFullSellOrder(topSellOrder.getOrderID());
		} else if (topSellOrder.getQuantity() > tradeDto.getQuantityFilled()){
			fillPartialSellOrder(topSellOrder.getOrderID(), tradeDto.getQuantityFilled());
		}
		
		// Return trade
		return tradeDao.getTradeById(tradeId);
	}
	
	/**
	 * Checks that the bid-ask spread (amount by which the ask price exceeds the bid price)
	 * satisfies the minimum value (-0.1, when the ask price is 0.1 below the bid price)
	 * @param buyOrder
	 * @param sellOrder
	 * @return
	 */
	private boolean bidPriceMeetsAskPrice(Order buyOrder, Order sellOrder) {
		// Spread = Bid - Ask
		BigDecimal spread = buyOrder.getPrice().subtract(sellOrder.getPrice());

		// Spread cannot be less than -0.1
		if (spread.compareTo(MINIMUM_BID_ASK_SPREAD) < 0) {
			return false;
		}
		
		return true;
	}
	
	private void fillFullBuyOrder(String buyOrderId) throws OrderbookPersistenceException {
		orderDao.removeBuyOrder(buyOrderId);
	}
	
	private void fillFullSellOrder(String sellOrderId) throws OrderbookPersistenceException {
		orderDao.removeSellOrder(sellOrderId);
	}
	
	private void fillPartialBuyOrder(String buyOrderId, int quantityFilled) throws OrderbookPersistenceException {
		
		Order order = orderDao.getBuyOrder(buyOrderId);
		
		// Update order quantity
		order.setQuantity(order.getQuantity() - quantityFilled);
		
		orderDao.editBuyOrder(buyOrderId, order);
	}
	
	private void fillPartialSellOrder(String sellOrderId, int quantityFilled) throws OrderbookPersistenceException {
		
		Order order = orderDao.getSellOrder(sellOrderId);
		
		// Update order quantity
		order.setQuantity(order.getQuantity() - quantityFilled);
		
		orderDao.editSellOrder(sellOrderId, order);
	}
	
	@Override
	public List<Trade> matchAll() throws OrderbookPersistenceException {
		List<Trade> trades = new ArrayList<>();
		
		Trade currentTrade;
		do {
			currentTrade = match();
			
			if (currentTrade != null) {
				trades.add(currentTrade);
			}
			
		} while (currentTrade != null);
		
		return trades;
	}
	
	private boolean orderbookIsEmpty() throws OrderbookPersistenceException {
		if (orderDao.getAllBuyOrders().isEmpty() || orderDao.getAllSellOrders().isEmpty()) {
			return true;
		}
		return false;
	}

	@Override
	public Order addBuyOrder(OrderDto orderDto) throws InvalidOrderException, OrderbookPersistenceException {

		// Treat null argument
		if (orderDto == null) return null;

		// Validate Order DTO
		if (orderDto.getPrice().compareTo(MIN_ORDER_PRICE) < 0
				|| orderDto.getPrice().compareTo(MAX_ORDER_PRICE) > 0) {
			throw new InvalidOrderException("Order price must be between 190.00 and 191.00");
		}

		if (orderDto.getQuantity() < MIN_ORDER_QUANTITY
				|| orderDto.getQuantity() > MAX_ORDER_QUANTITY) {
			throw new InvalidOrderException("Order quantity must be between 20 and 50");
		}

		if (!orderDto.getOrderType().equals(OrderType.BUY)) {
			throw new InvalidOrderException("Expected a buy order");
		}

		// Get generated order by ID
		String addedOrderId = orderDao.addBuyOrder(orderDto);

		return orderDao.getBuyOrder(addedOrderId);
	}

	@Override
	public Order addSellOrder(OrderDto orderDto) throws InvalidOrderException, OrderbookPersistenceException {

		// Treat null argument
		if (orderDto == null) return null;

		// Validate Order DTO
		if (orderDto.getPrice().compareTo(MIN_ORDER_PRICE) < 0
				|| orderDto.getPrice().compareTo(MAX_ORDER_PRICE) > 0) {
			throw new InvalidOrderException("Order price must be between 190.00 and 191.00");
		}

		if (orderDto.getQuantity() < MIN_ORDER_QUANTITY
				|| orderDto.getQuantity() > MAX_ORDER_QUANTITY) {
			throw new InvalidOrderException("Order quantity must be between 20 and 50");
		}

		if (!orderDto.getOrderType().equals(OrderType.SELL)) {
			throw new InvalidOrderException("Expected a sell order");
		}

		// Get generated order by ID
		String addedOrderId = orderDao.addSellOrder(orderDto);

		return orderDao.getSellOrder(addedOrderId);
	}

	@Override
	public Order editBuyOrder(String buyOrderId, Order editedBuyOrder)
			throws OrderNotFoundException, InvalidOrderException, OrderbookPersistenceException {

		// Treat null arguments
		if (buyOrderId == null || editedBuyOrder == null) return null;

		// Make sure order exists
		Order oldOrder = orderDao.getBuyOrder(buyOrderId);

		if (oldOrder == null) {
			throw new OrderNotFoundException("ID doesn't match any existing buy order");
		}

		// Validate edited Order DTO
		if (editedBuyOrder.getPrice().compareTo(MIN_ORDER_PRICE) < 0
				|| editedBuyOrder.getPrice().compareTo(MAX_ORDER_PRICE) > 0) {
			throw new InvalidOrderException("Order price must be between 190.00 and 191.00");
		}

		if (editedBuyOrder.getQuantity() < MIN_ORDER_QUANTITY
				|| editedBuyOrder.getQuantity() > MAX_ORDER_QUANTITY) {
			throw new InvalidOrderException("Order quantity must be between 20 and 50");
		}

		if (!editedBuyOrder.getOrderType().equals(OrderType.BUY)) {
			throw new InvalidOrderException("Expected a buy order");
		}

		// Return edited order (new) Order
		return orderDao.editBuyOrder(buyOrderId, editedBuyOrder);
	}

	@Override
	public Order editSellOrder(String sellOrderId, Order editedSellOrder)
			throws OrderNotFoundException, InvalidOrderException, OrderbookPersistenceException {

		// Treat null arguments
		if (sellOrderId == null || editedSellOrder == null) return null;

		// Make sure order exists
		Order oldOrder = orderDao.getSellOrder(sellOrderId);

		if (oldOrder == null) {
			throw new OrderNotFoundException("ID doesn't match any existing sell order");
		}

		// Validate edited Order DTO
		if (editedSellOrder.getPrice().compareTo(MIN_ORDER_PRICE) < 0
				|| editedSellOrder.getPrice().compareTo(MAX_ORDER_PRICE) > 0) {
			throw new InvalidOrderException("Order price must be between 190.00 and 191.00");
		}

		if (editedSellOrder.getQuantity() < MIN_ORDER_QUANTITY
				|| editedSellOrder.getQuantity() > MAX_ORDER_QUANTITY) {
			throw new InvalidOrderException("Order quantity must be between 20 and 50");
		}

		if (!editedSellOrder.getOrderType().equals(OrderType.SELL)) {
			throw new InvalidOrderException("Expected a sell order");
		}

		// Return edited order (new) Order
		return orderDao.editSellOrder(sellOrderId, editedSellOrder);
	}

	@Override
	public Order getBuyOrderById(String buyOrderId) throws OrderbookPersistenceException {
		return orderDao.getBuyOrder(buyOrderId);
	}

	@Override
	public Order getSellOrderById(String sellOrderId) throws OrderbookPersistenceException {
		return orderDao.getSellOrder(sellOrderId);
	}
	
	@Override
	public Trade getTradeById(String tradeId) throws OrderbookPersistenceException {
		return tradeDao.getTradeById(tradeId);
	}

	@Override
	public List<Trade> getAllTradesSortedByExecutionTime() throws OrderbookPersistenceException {
		return tradeDao.getAllTrades()
				.stream()
				.sorted((t1, t2) -> t2.getExecutionTime().compareTo(t1.getExecutionTime()))
				.collect(Collectors.toList());
	}

	@Override
	public Order removeBuyOrder(String buyOrderId) throws OrderNotFoundException, OrderbookPersistenceException {
		// Treat null argument
		if (buyOrderId == null) return null;

		// Make sure order exists
		Order oldOrder = orderDao.getBuyOrder(buyOrderId);

		if (oldOrder == null) {
			throw new OrderNotFoundException("ID doesn't match any existing buy order");
		}

		// Return removed Order
		return orderDao.removeBuyOrder(buyOrderId);
	}

	@Override
	public Order removeSellOrder(String sellOrderId) throws OrderNotFoundException, OrderbookPersistenceException {
		// Treat null argument
		if (sellOrderId == null) return null;

		// Make sure order exists
		Order oldOrder = orderDao.getSellOrder(sellOrderId);

		if (oldOrder == null) {
			throw new OrderNotFoundException("ID doesn't match any existing sell order");
		}

		// Return removed Order
		return orderDao.removeSellOrder(sellOrderId);
	}

}
