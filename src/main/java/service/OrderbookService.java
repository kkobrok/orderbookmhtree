package service;

import dto.OrderDto;
import dto.OrderbookStats;
import exceptions.InvalidOrderException;
import exceptions.OrderNotFoundException;
import exceptions.OrderbookPersistenceException;
import model.Order;
import model.Trade;

import java.util.List;

public interface OrderbookService {


    OrderbookStats getOrderbookStats() throws OrderbookPersistenceException;
    List<Order> getAllBuyOrders() throws OrderbookPersistenceException;
    List<Order> getAllSellOrders() throws OrderbookPersistenceException;
    Trade match() throws OrderbookPersistenceException;
    List<Trade> matchAll() throws OrderbookPersistenceException;
    Order addBuyOrder(OrderDto orderDto) throws InvalidOrderException, OrderbookPersistenceException;
    Order addSellOrder(OrderDto orderDto) throws InvalidOrderException, OrderbookPersistenceException;
    Order editBuyOrder(String buyOrderId, Order editedBuyOrder) throws OrderNotFoundException, InvalidOrderException, OrderbookPersistenceException;
    Order editSellOrder(String sellOrderId, Order editedBuyOrder) throws OrderNotFoundException, InvalidOrderException, OrderbookPersistenceException;
    Order removeBuyOrder(String buyOrderId) throws OrderNotFoundException, OrderbookPersistenceException;
    Order removeSellOrder(String sellOrderId) throws OrderNotFoundException, OrderbookPersistenceException;
    Order getBuyOrderById(String buyOrderId) throws OrderbookPersistenceException;
    Order getSellOrderById(String sellOrderId) throws OrderbookPersistenceException;
    Trade getTradeById(String tradeId) throws OrderbookPersistenceException;
    List<Trade> getAllTradesSortedByExecutionTime() throws OrderbookPersistenceException;

}
